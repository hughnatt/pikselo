import React, { useCallback } from 'react';

interface IEditTextProps {
    id?: string,
    value: string,
    label?: string,
    onChange: (value: string) => void,
    type?: 'text' | 'password' | 'number',
}

export const EditText = ({
    id,
    value,
    label,
    onChange,
    type = 'text',
}: IEditTextProps) => {
    const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        onChange(event.target.value);
    }, [onChange]);

    return (
        <label>
            {label}
            <input
                id={id}
                type={type}
                value={value}
                onChange={handleChange}
            />
        </label>
    );
};
