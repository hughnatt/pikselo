import { useCallback, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useStore } from '../../../contexts/Store';

interface ILogoutProps {
    path: string,
}

export const Logout = ({
    path,
}: ILogoutProps) => {
    const { dispatch } = useStore();
    const history = useHistory();
    const [success, setSuccess] = useState(false);

    useEffect(() => {
        dispatch({ type: 'UPDATE_SESSION', session: undefined});
        setSuccess(true);
    }, [dispatch, history]);

    const handleRedirect = useCallback(() => {
        history.push(`${path}/login`);
    }, [history, path]);

    return (
        <>
            {
                success && (
                    <div>
                        <h4>You've been logged out.</h4>
                        <Button onClick={handleRedirect}>Click here to login.</Button>
                    </div>
                )
            }
        </>
    );
};
