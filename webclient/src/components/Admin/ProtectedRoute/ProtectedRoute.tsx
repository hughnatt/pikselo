import { Redirect, Route } from 'react-router-dom'
import { useStore } from '../../../contexts/Store';

interface IProtectedRouteProps {
    path: string,
    exact?: boolean,
}

export const ProtectedRoute: React.FC<IProtectedRouteProps> = ({
    path,
    exact,
    children,
}) => {
    const { store } = useStore();

    return (
        store.session
            ? <Route exact={exact} path={path}>{children}</Route>
            : <Redirect to="/admin/login" />
    );
};
