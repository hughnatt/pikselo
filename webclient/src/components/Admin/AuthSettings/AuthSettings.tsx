import { useCallback, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { publicJoinUrl } from '../../../constants';
import { useAuthSettingsApi } from '../../../data/useAuthSettingsApi';
import { useNotifier } from '../../../hooks/useNotifier';
import { AuthSettingsInfo } from '../../../types/AuthSettingsInfo';

export const AuthSettings = () => {
    const [authSettings, setAuthSettings] = useState<AuthSettingsInfo>();
    const { getAuthSettings, updateAuthSettings } = useAuthSettingsApi();
    const { postError } = useNotifier();

    useEffect(() => {
        getAuthSettings()
            .then((authSettingsData) => {
                setAuthSettings(authSettingsData.authSettings);
            })
            .catch((error) => {
                postError('Failed to get auth settings', error);
            });
    }, [getAuthSettings, postError]);

    const handleTogglePublicAuth = useCallback(() => {
        if (authSettings) {
            updateAuthSettings({
                ...authSettings,
                publicAuthEnabled: !authSettings.publicAuthEnabled,
            })
                .then((authSettingsData) => {
                    setAuthSettings(authSettingsData.authSettings);
                })
                .catch((error) => {
                    postError('Failed to updated auth settings', error);
                });
        }
    }, [updateAuthSettings, postError, authSettings]);

    const handleToggleQRAuth = useCallback(() => {
        if (authSettings) {
            updateAuthSettings({
                ...authSettings,
                qrAuthEnabled: !authSettings.qrAuthEnabled,
            })
                .then((authSettingsData) => {
                    setAuthSettings(authSettingsData.authSettings);
                })
                .catch((error) => {
                    postError('Failed to updated auth settings', error);
                });
        }
    }, [updateAuthSettings, postError, authSettings]);

    return (
        (authSettings) ? (
            <div>
                <div>
                    <h3>
                        {
                            (authSettings.publicAuthEnabled)
                                ? 'Public authentication is enabled'
                                : 'Public authentication is disabled'
                        }
                    </h3>
                    <Button onClick={handleTogglePublicAuth}>TOGGLE</Button>
                    {
                        (authSettings.publicAuthEnabled) && (
                            <h3><a href={publicJoinUrl} target="_blank" rel="noopener noreferrer">{publicJoinUrl}</a></h3>
                        )
                    }
                </div>
                <div>
                    <h3>
                        {
                            (authSettings.qrAuthEnabled)
                                ? 'QR authentication is enabled'
                                : 'QR authentication is disabled'
                        }
                    </h3>
                    <Button onClick={handleToggleQRAuth}>TOGGLE</Button>
                </div>
            </div>
        ) : null
    );
};
