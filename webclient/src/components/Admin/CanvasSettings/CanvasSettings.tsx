import { useCallback, useEffect, useState } from 'react';
import { Button, Container, Form, Col, Row } from 'react-bootstrap';
import { useBoardApi } from '../../../data/useBoardApi';
import { useCanvasSettingsApi } from '../../../data/useCanvasSettingsApi';
import { useNotifier } from '../../../hooks/useNotifier';
import { CanvasSettingsInfo } from '../../../types/CanvasSettingsInfo';

export const CanvasSettings = () => {
    const { getCanvasSettings, updateCanvasSettings } = useCanvasSettingsApi();
    const { cleanBoard } = useBoardApi();
    const [canvasSettings, setCanvasSettings] = useState<Partial<CanvasSettingsInfo>>({});
    const { postError, postInfo } = useNotifier();

    useEffect(() => {
        getCanvasSettings()
            .then((canvasSettingsData) => {
                setCanvasSettings(canvasSettingsData.canvasSettings);
            })
            .catch((error) => {
                postError('Failed to get canvas settings', error);
            })
    }, [getCanvasSettings, postError]);

    const handleUpdate = useCallback(() => {
        updateCanvasSettings(canvasSettings)
            .then((canvasSettingsData) => {
                setCanvasSettings(canvasSettingsData.canvasSettings);
            })
            .catch((error) => {
                postError('Failed to update canvas settings', error);
            })
    }, [updateCanvasSettings, canvasSettings, postError]);

    const handleCooldownChange = useCallback((value: string) => {
        setCanvasSettings({
            ...canvasSettings,
            cooldown: parseInt(value),
        });
    }, [canvasSettings]);

    const handleBoardWidthChange = useCallback((value: string) => {
        setCanvasSettings({
            ...canvasSettings,
            boardWidth: parseInt(value),
        });
    }, [canvasSettings]);

    const handleBoardHeightChange = useCallback((value: string) => {
        setCanvasSettings({
            ...canvasSettings,
            boardHeight: parseInt(value),
        });
    }, [canvasSettings]);

    const handleReadOnlyChange = useCallback(() => {
        setCanvasSettings({
            ...canvasSettings,
            readOnly: !canvasSettings.readOnly,
        })
    }, [canvasSettings]);

    const handleClean = useCallback(() => (
        cleanBoard()
            .then(() => {
                postInfo('board cleaned');
            })
            .catch((error) => {
                postError('Failed to clean board', error);
            })
    ), [cleanBoard, postInfo, postError]);

    return (
        <div>
            {
                canvasSettings && (

                    <Container fluid="sm">
                        <Row className="justify-content-md-center">
                            <Col xs lg="4">
                                <Form>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Cooldown</Form.Label>
                                        <Form.Control
                                            type="number"
                                            value={`${canvasSettings.cooldown}`}
                                            onChange={(e) => handleCooldownChange(e.target.value)}
                                        />
                                    </Form.Group>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Board height</Form.Label>
                                        <Form.Control
                                            type="number"
                                            value={`${canvasSettings.boardHeight}`}
                                            onChange={(e) => handleBoardHeightChange(e.target.value)}
                                        />
                                    </Form.Group >
                                    <Form.Group className="mb-3">
                                        <Form.Label>Board width</Form.Label>
                                        <Form.Control
                                            type="number"
                                            value={`${canvasSettings.boardWidth}`}
                                            onChange={(e) => handleBoardWidthChange(e.target.value)}
                                        />
                                    </Form.Group>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Read only</Form.Label>
                                        <Form.Check
                                            checked={canvasSettings.readOnly}
                                            type="switch"
                                            onChange={handleReadOnlyChange}
                                        />
                                    </Form.Group>
                                    <Button onClick={handleUpdate}>Update</Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                )
            }
            <div>
                <h1>Clean Board</h1>
                <h6>This action is irreversible</h6>
                <Button onClick={handleClean} variant="danger">Clean</Button>
            </div>
        </div>
    );
};
