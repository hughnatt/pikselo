import './Admin.css';

import { Route, Switch, useRouteMatch } from "react-router-dom"
import { StoreProvider } from "../../contexts/Store";
import { AppNavbar } from "./AppNavbar/AppNavbar";
import { AuthSettings } from "./AuthSettings/AuthSettings";
import { CanvasSettings } from "./CanvasSettings/CanvasSettings";
import { Dashboard } from "./Dashboard/Dashboard";
import { Login } from "./Login/Login";
import { Logout } from "./Logout/Logout";
import { ProtectedRoute } from "./ProtectedRoute/ProtectedRoute";
import { QRAuth } from "./QRAuth/QRAuth";
import { UserManagement } from "./UserManagement/UserManagement";
import { publicUrl } from "../../constants";
import { PixelAnalyzer } from './PixelAnalyzer/PixelAnalyzer';
import { GodMode } from './GodMode/GodMode';

export const Admin = () => {
    const { path } = useRouteMatch();

    return (
        <div className="Admin">
            <StoreProvider>
                <AppNavbar baseUrl={`${publicUrl}${path}`} />
                <Switch>
                    <Route exact path={`${path}/login`}>
                        <Login path={path}/>
                    </Route>
                    <ProtectedRoute exact path={path}>
                        <Dashboard />
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/users/manage`}>
                        <UserManagement />
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/canvas/settings`}>
                        <CanvasSettings />
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/canvas/analyzer`}>
                        <PixelAnalyzer />
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/canvas/godmode`}>
                        <GodMode />
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/logout`}>
                        <Logout path={path}/>
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/auth/qrauth`}>
                        <QRAuth />
                    </ProtectedRoute>
                    <ProtectedRoute exact path={`${path}/auth/settings`}>
                        <AuthSettings />
                    </ProtectedRoute>
                </Switch>
            </StoreProvider>
        </div>
    );
};