import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';

interface IAppNavbarProps {
    baseUrl: string,
}

export const AppNavbar= ({
    baseUrl,
}: IAppNavbarProps) => {
    return (
        <Navbar collapseOnSelect expand="lg">
            <Container>
                <Navbar.Brand href={baseUrl}>Admin Panel</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href={baseUrl}>Dashboard</Nav.Link>
                        <NavDropdown title="Users" id="users-nav-dropdown">
                            <NavDropdown.Item href={`${baseUrl}/users/manage`}>Manage</NavDropdown.Item>
                            <NavDropdown.Item href={`${baseUrl}/users/createAdmin`}>Create admin</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Canvas" id="canvas-nav-dropdown">
                            <NavDropdown.Item href={`${baseUrl}/canvas/settings`}>Settings</NavDropdown.Item>
                            <NavDropdown.Item href={`${baseUrl}/canvas/analyzer`}>Pixel analyzer</NavDropdown.Item>
                            <NavDropdown.Item href={`${baseUrl}/canvas/godmode`}>God mode</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Auth" id="auth-nav-dropdown">
                            <NavDropdown.Item href={`${baseUrl}/auth/settings`}>Settings</NavDropdown.Item>
                            <NavDropdown.Item href={`${baseUrl}/auth/qrauth`}>QRAuth</NavDropdown.Item>
                        </NavDropdown>
                        <Nav.Link href={`${baseUrl}/logout`}>Logout</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};
