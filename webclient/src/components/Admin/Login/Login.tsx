import { useCallback, useState } from 'react';
import { Button, Container, Form, Row, Col } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useStore } from '../../../contexts/Store';
import { useAuthApi } from '../../../data/useAuthApi';
import { useNotifier } from '../../../hooks/useNotifier';

interface ILoginProps {
    path: string,
}

export const Login = ({
    path,
}: ILoginProps) => {
    const [userField, setUserField] = useState('');
    const [passwordField, setPasswordField] = useState('');
    const { loginWithPassword } = useAuthApi();
    const { dispatch } = useStore();
    const { postError } = useNotifier();
    const history = useHistory();

    const handleSubmit = useCallback((e) => {
        loginWithPassword(userField, passwordField)
            .then((session) => {
                dispatch({ type: 'UPDATE_SESSION', session})
                history.push(path);
            })
            .catch((error) => {
                postError('LOGIN FAILED', error);
            });
    }, [loginWithPassword, dispatch, passwordField, userField, postError, history, path]);

    return (
        <Container fluid="sm">
            <Row className="justify-content-md-center">
                <Col xs lg="4">
                    <Form>
                        <Form.Group className="sm-3" controlId="loginFormUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                name="username"
                                type="username"
                                placeholder="Enter username"
                                onChange={(e) => setUserField(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className="sm-3" controlId="loginFormPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                name="password"
                                type="password"
                                placeholder="Password"
                                onChange={(e) => setPasswordField(e.target.value)}
                            />
                        </Form.Group>
                        <Button
                            variant="primary"
                            onClick={handleSubmit}
                        >
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};
