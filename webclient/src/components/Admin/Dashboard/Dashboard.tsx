import { useEffect, useState } from 'react';
import { useDashboardApi } from '../../../data/useDashboardApi';
import { useNotifier } from '../../../hooks/useNotifier';
import { DashboardInfo } from '../../../types/DashboardInfo';

export const Dashboard = () => {
    const { getDashboard } = useDashboardApi();
    const [dashboard, setDashboard] = useState<DashboardInfo>();
    const { postError } = useNotifier();

    useEffect(() => {
        getDashboard()
            .then((dashboardData) => {
                setDashboard(dashboardData.dashboard);
            }).catch((error) => {
                postError('Failed to get dashboard', error);
            });
    }, [getDashboard, postError]);

    return (
        <div>
            <h1>Admin Panel</h1>
            <h3>Total user count: {dashboard?.userCount}</h3>
        </div>
    );
};
