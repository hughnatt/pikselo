import { useCallback } from "react";
import { Button, Modal } from "react-bootstrap";
import { formatDate } from "../../../../helpers/formatDate";
import { useBanUser } from "../../../../hooks/useBanUser";

import { useGetSinglePixelInfo } from "../../../../hooks/useGetSinglePixelInfo";
import { useTranslation } from "../../../../i18n/i18n";

interface IPixelAnalyzerPopupProps {
    position?: {
        x: number,
        y: number
    },
    onClose: () => void,
}

export const PixelAnalyzerPopup = ({
    position,
    onClose,
}: IPixelAnalyzerPopupProps) => {
    const { t } = useTranslation();
    const { singlePixelInfo } = useGetSinglePixelInfo({ position });
    const { banUser } = useBanUser();

    const handleClick = useCallback(() => {
        if (singlePixelInfo?.userId) {
            banUser(singlePixelInfo.userId);
        }
    }, [banUser, singlePixelInfo]);

    return (
        <Modal
            show={position !== undefined}
            onHide={onClose}
            backdrop="static"
        >
            <Modal.Header>
                <Modal.Title>{t('admin.analyser.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    {singlePixelInfo?.userId}
                </p>
                <Button color="error" onClick={handleClick}>{t('admin.analyzer.button.ban')}</Button>
                <p>
                    {singlePixelInfo?.lastDrawTime && formatDate(new Date(singlePixelInfo.lastDrawTime))}
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={onClose}>{t('admin.analyser.button.close')}</Button>
            </Modal.Footer>
        </Modal>
    );
};