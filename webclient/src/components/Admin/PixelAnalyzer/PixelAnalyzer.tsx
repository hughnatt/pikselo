import './PixelAnalyzer.css';

import { useCallback, useEffect, useRef, useState } from "react";
import { useGetBoard } from "../../../hooks/useGetBoard";
import { PixelCanvas } from "../../Game/PixelCanvas/PixelCanvas";
import { PixelAnalyzerPopup } from './PixelAnalyserPopup/PixelAnalyzerPopup';

export const PixelAnalyzer = () => {
    const internalCanvasRef = useRef<HTMLCanvasElement>(null);
    const [imageData, setImageData] = useState<ImageData>();
    const [canvasHeight, setCanvasHeight] = useState(0);
    const [canvasWidth, setCanvasWidth] = useState(0);
    const [toggleRecenter] = useState(false);
    const [position, setPosition] = useState<{x: number, y: number}>();

    const { getBoard } = useGetBoard('', true);

    const handlePixelSelected = useCallback((x: number, y: number) => {
        setPosition({ x, y });
    }, []);

    const handleClosePopup = useCallback(() => {
        setPosition(undefined);
    }, []);

    useEffect(() => {
        const internalContext = internalCanvasRef.current?.getContext('2d');

        getBoard(internalContext)
            .then((board) => {
                setImageData(board.imageData);
                setCanvasHeight(board.height);
                setCanvasWidth(board.width);
            })
    }, [getBoard]);

    return (
        <div className="board">
            <PixelCanvas
                internalCanvasRef={internalCanvasRef}
                imageData={imageData}
                canvasHeight={canvasHeight}
                canvasWidth={canvasWidth}
                onPixelClick={handlePixelSelected}
                toggleRecenter={toggleRecenter}
            />
            <PixelAnalyzerPopup position={position} onClose={handleClosePopup} />
        </div>
    );
};