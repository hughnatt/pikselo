import { publicUrl } from "../../../constants";
import { useStore } from "../../../contexts/Store";

export const GodMode = () => {
    const { store : { session } } = useStore()

    return (
        <a href={`${publicUrl}/play/${session?.accessToken}`}>Access god mode</a>
    );
};