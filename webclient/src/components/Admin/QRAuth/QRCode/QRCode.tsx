import QRCodeReact from 'qrcode.react';
import './QRCode.css';

interface IQRCodeProps {
    value: string,
}

export const QRCode = ({
    value,
}: IQRCodeProps) => (
    <QRCodeReact className="qrcode" renderAs="svg" value={value} />
);
