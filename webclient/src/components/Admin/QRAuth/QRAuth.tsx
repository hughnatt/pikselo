import { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import { apiBasePath } from '../../../constants';
import { useStore } from '../../../contexts/Store';
import { useNotifier } from '../../../hooks/useNotifier';
import { QRCode } from './QRCode/QRCode';
import { useTranslation } from '../../../i18n/i18n';
import './QRAuth.css';

export const QRAuth = () => {
    const { postError } = useNotifier();
    const [url, setUrl] = useState<string>();
    const { store } = useStore();
    const { t } = useTranslation();

    useEffect(() => {
        const socket = io(`${apiBasePath}/qrauth`, {
            auth: { token: store.session?.accessToken }
        });

        socket.on('refresh-qrcode', (data) => {
            setUrl(data.url);
        });

        return () => {
            socket.off('refresh-qrcode');
        };
    }, [postError, store.session]);

    return (
        url ? (
            <div className="qrauth">
                <div className="flex">
                    <a className="link" href={url} target="_blank" rel="noreferrer">
                        <div className="qrcode-wrapper">
                            <QRCode value={url}/>
                        </div>
                    </a>
                    <h1>
                        {t('admin.qrauth.title')}
                    </h1>
                </div>
            </div>
        ) : null
    )
};
