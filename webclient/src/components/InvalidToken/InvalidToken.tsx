import { Modal } from "react-bootstrap";
import { useTranslation } from "../../i18n/i18n";

export const InvalidToken = () => {
    const { t } = useTranslation();

    return (
        <Modal
            show={true}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
            <Modal.Title>{t('error.invalidtoken.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {t('error.invalidtoken.description')}
            </Modal.Body>
        </Modal>
    );
};