import { useRef, RefObject, useCallback, useEffect, useState } from 'react';
import { getViewport } from '../../../helpers/getViewport';
import { useHammer } from '../../../hooks/useHammer';
import { usePixelListener } from '../../../hooks/usePixelListener';
import './PixelCanvas.css';

interface IPixelCanvasProps {
    internalCanvasRef: RefObject<HTMLCanvasElement>,
    canvasHeight: number,
    canvasWidth: number,
    imageData?: ImageData,
    onPixelClick: (x: number, y: number) => void,
    toggleRecenter: unknown,
}

export const PixelCanvas = ({
    internalCanvasRef,
    canvasHeight,
    canvasWidth,
    imageData,
    onPixelClick,
    toggleRecenter,
}: IPixelCanvasProps) => {
    const [windowHeight, setWindowHeight] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);

    const userCanvasRef = useRef<HTMLCanvasElement>(null);
    const { refresh } = usePixelListener(canvasWidth, imageData);

    const {
        canvasScale,
        canvasOffset,
    } = useHammer({
        userCanvas: userCanvasRef.current,
        windowHeight,
        windowWidth,
        canvasHeight,
        canvasWidth,
        onPixelClick,
        toggleRecenter,
    });

    const resizeCanvas = useCallback(() => {
        setWindowWidth(window.innerWidth);
        setWindowHeight(window.innerHeight);
    }, []);

    const setupAutoResize = useCallback(() => {
        window.addEventListener('resize', resizeCanvas, false);
        window.addEventListener('orientationchange', resizeCanvas, false);
        resizeCanvas();
    }, [resizeCanvas]);

    const draw = useCallback(() => {
        const internalCanvas = internalCanvasRef.current;
        const userCanvas = userCanvasRef.current;

        if (internalCanvas && userCanvas) {
            internalCanvas.width = canvasWidth;
            internalCanvas.height = canvasHeight;

            const internalContext = internalCanvas.getContext('2d');
            const userContext = userCanvas.getContext('2d');

            if (internalContext && userContext && imageData) {
                internalContext.putImageData(imageData, 0, 0)
                userContext.clearRect(0, 0, windowWidth, userContext.canvas.height)
                const viewport = getViewport(
                    canvasWidth,
                    canvasHeight,
                    windowWidth,
                    windowHeight,
                    canvasScale,
                    canvasOffset,
                );
                userContext.imageSmoothingEnabled = false
                try {
                    userContext.drawImage(
                        internalCanvas,
                        viewport.x, viewport.y, viewport.width, viewport.height,
                        0, 0, userContext.canvas.width, userContext.canvas.height)
                } catch (error) {
                    // Nothing here.
                }
            }
        }
    }, [canvasHeight, canvasWidth, internalCanvasRef, userCanvasRef, canvasOffset, canvasScale, imageData, windowHeight, windowWidth]);

    useEffect(() => {
        draw();
      }, [draw, refresh]);

      useEffect(() => {
          setupAutoResize();
      }, [setupAutoResize]);

    return (
        <>
            <canvas ref={userCanvasRef} id="user-canvas" width={windowWidth} height={windowHeight}>Incompatible browser</canvas>
            <canvas ref={internalCanvasRef} id="internal-canvas"></canvas>
        </>
    )
}

