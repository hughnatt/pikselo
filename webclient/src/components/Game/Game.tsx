import './Game.css';

import { useCallback, useEffect, useRef, useState } from 'react';

import { Palette } from './Palette/Palette';
import { usePaintColor } from '../../hooks/usePaintColor';
import { Countdown } from './Countdown/Countdown';
import { Toolbar } from './Toolbar/Toolbar';
import { useGetBoard } from '../../hooks/useGetBoard';
import { PixelCanvas } from './PixelCanvas/PixelCanvas';
import { useParams } from 'react-router-dom';
import { EulaPopup } from './EulaPopup/EulaPopup';
import { useRefreshListener } from '../../hooks/useRefreshListener';
import { LoadingScreen } from './LoadingScreen/LoadingScreen';
import { ReadOnlyPopup } from './ReadOnlyPopup/ReadOnlyPopup';
import { ErrorScreen } from './ErrorScreen/ErrorScreen';
import { SharePopup } from './SharePopup/SharePopup';

interface IGameParams {
    token: string,
}

export const Game = () => {
    const { token } = useParams<IGameParams>();
    const internalCanvasRef = useRef<HTMLCanvasElement>(null);

    const [cooldownDelay, setCooldownDelay] = useState<number>();
    const [readOnly, setReadOnly] = useState(false);

    const { getBoard } = useGetBoard(token);
    const { refresh } = useRefreshListener();

    const [color, setColor] = useState(0);

    const { paintColor } = usePaintColor(token, color, setCooldownDelay, readOnly);

    const [imageData, setImageData] = useState<ImageData>();
    const [canvasHeight, setCanvasHeight] = useState(0);
    const [canvasWidth, setCanvasWidth] = useState(0);
    const [toggleRecenter, setToggleRecenter] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [showColorPalette, setShowColorPalette] = useState(true);
    const [showSharePopup, setShowSharePopup] = useState(false);

    const handleRecenter = useCallback(() => {
        setToggleRecenter((recenter) => !recenter);
    }, []);

    const handleTogglePalette = useCallback(() => {
        setShowColorPalette((showColorPalette) => !showColorPalette);
    }, []);

    const handleShare = useCallback(() => {
        setShowSharePopup(true);
    }, []);

    useEffect(() => {
        const internalContext = internalCanvasRef.current?.getContext('2d');

        setLoading(true);
        getBoard(internalContext)
            .then((board) => {
                setImageData(board.imageData);
                setCanvasHeight(board.height);
                setCanvasWidth(board.width);
                setReadOnly(board.readOnly);
                handleRecenter();
            })
            .catch(() => {
                setError(true);
            })
            .finally(() => {
                setLoading(false);
            });
    }, [getBoard, refresh, handleRecenter]);

    return (
        <div className="game">
            {
                loading && (
                    <LoadingScreen />
                )
            }
            {
                !loading && error && (
                    <ErrorScreen />
                )
            }
            {
                !loading && !error && (
                    <>
                        <PixelCanvas
                            internalCanvasRef={internalCanvasRef}
                            imageData={imageData}
                            canvasHeight={canvasHeight}
                            canvasWidth={canvasWidth}
                            onPixelClick={paintColor}
                            toggleRecenter={toggleRecenter}
                        />
                        <Countdown delay={cooldownDelay} />
                        <Toolbar
                            handleRecenter={handleRecenter}
                            handleTogglePalette={handleTogglePalette}
                            handleShare={handleShare}
                            color={color}
                            readOnly={readOnly}
                        />
                        {
                            readOnly && <ReadOnlyPopup />
                        }
                        {
                            !readOnly && 
                                <>
                                    <EulaPopup token={token} setCooldownDelay={setCooldownDelay} />
                                    <SharePopup open={showSharePopup} onClose={() => setShowSharePopup(false)}/>
                                </>
                        }
                    </>
                )
            }
            <Palette
                onColorChange={setColor}
                hidden={loading || readOnly || error || !showColorPalette}
            />
        </div>
    );
};
