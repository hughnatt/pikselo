import { Button, ListGroup } from 'react-bootstrap';
import { getCSSColor } from '../../../helpers/convertColor';
import './Toolbar.css';

interface IToolbarProps {
    handleRecenter: () => void,
    handleTogglePalette: () => void,
    handleShare: () => void,
    color: number,
    readOnly: boolean,
}

export const Toolbar = ({
    handleRecenter,
    handleTogglePalette,
    handleShare,
    color,
    readOnly,
}: IToolbarProps) => {
    return (
        <div className="recenter">
            <ListGroup>
                <ListGroup.Item className="item">
                    <Button onClick={handleRecenter} className="button">
                        <i className="fa fa-arrows-alt"></i>
                    </Button>
                </ListGroup.Item>
                {
                    !readOnly &&
                    <>
                        <ListGroup.Item className="item">
                            <Button
                                onClick={
                                    (e) => {
                                        e.preventDefault();
                                        e.stopPropagation();
                                        handleTogglePalette();
                                    }
                                }
                                className="button"
                                style={
                                    {
                                        backgroundColor: getCSSColor(color),
                                    }
                                }
                            />
                        </ListGroup.Item>
                        <ListGroup.Item className="item">
                            <Button onClick={handleShare} className="button">
                                <i className="fa fa-share-alt"></i>
                            </Button>
                        </ListGroup.Item>
                    </>
                }
            </ListGroup>
        </div>
    )
};
