import { useEffect, useCallback, useState } from 'react';
import './Countdown.css';
import { apiBasePath } from '../../../constants';

interface ICountdownProps {
    delay?: number,
}

interface ICountdownTimeLeft {
    minutes: number,
    seconds: number,
}

export const Countdown = ({ delay }: ICountdownProps) => {
    const [untilTime, setUntilTime] = useState<number>();

    const calculateTimeLeft = useCallback(() => {
        let timeLeft: ICountdownTimeLeft | undefined = undefined;

        if (!untilTime) {
            return timeLeft;
        }

        let difference = untilTime - Date.now();

        if (difference > 0) {
            timeLeft = {
                minutes: Math.floor((difference / 1000 / 60) % 60),
                seconds: Math.floor((difference / 1000) % 60)
            };
        }
        return timeLeft;
    }, [untilTime]);

    const [timeLeft, setTimeLeft] = useState<ICountdownTimeLeft | undefined>(calculateTimeLeft());

    useEffect(() => {
        if (delay) {
            setUntilTime(Date.now() + delay * 1000);
        } else {
            setUntilTime(undefined);
        }
    }, [delay]);

    useEffect(() => {
        const timeLeft = calculateTimeLeft();
        setTimeLeft(timeLeft);
        const timer = setInterval(() => {
            const timeLeft = calculateTimeLeft();
            setTimeLeft(timeLeft);
            if (!timeLeft) {
                clearInterval(timer);
            }
        }, 1000);

        return () => clearInterval(timer);
    }, [calculateTimeLeft]);

    const padDigit = (x: number) => {
        return String(x).padStart(2, '0');
    };

    return (
        <div className={`countdown ${timeLeft ? '' : 'countdown-hidden'}`}>
            <a href="http://example.com" target="_blank" rel="noreferrer">
                <img alt="brand-logo" src={`${apiBasePath}/static/assets/brand-cooldown.png`} />
            </a>
            {
                timeLeft && (
                    <span>
                        {padDigit(timeLeft.minutes)}:{padDigit(timeLeft.seconds)}
                    </span>
                )
            }
        </div>
    );
}
