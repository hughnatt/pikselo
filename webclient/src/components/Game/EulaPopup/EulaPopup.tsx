import { useEffect } from "react";
import { useCallback, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useGameApi } from "../../../data/useGameApi";
import { useNotifier } from "../../../hooks/useNotifier";
import { useTranslation } from "../../../i18n/i18n";

interface IEulaPopupProps {
    token: string,
    setCooldownDelay: (cooldown: number) => void,
}

export const EulaPopup = ({
    token,
    setCooldownDelay,
}: IEulaPopupProps) => {
    const [show, setShow] = useState(false);
    const [email, setEmail] = useState<string>();
    const { getStatus, acceptEula } = useGameApi(token);
    const { postError } = useNotifier();
    const { t } = useTranslation();

    const handleClose = useCallback(() => {
        setShow(false);
    }, []);

    const handleAccept = useCallback(() => {
        acceptEula(email)
            .catch((error) => {
                postError('Failed to accept EULA. Retry later.', error);
            });
        setShow(false);
    }, [acceptEula, postError, email]);

    useEffect(() => {
        getStatus()
            .then(({ eulaAccepted, cooldown }) => {
                setCooldownDelay(cooldown);
                if (!eulaAccepted) {
                    setShow(true);
                }
            })
            .catch(() => {
                // Nothing here
            })
    }, [getStatus, setCooldownDelay]);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>{t('game.eula.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <p>
                {t('game.eula.description')}
            </p>
            <hr />
            <Form>
                <Form.Group className="mb-3" controlId="eulaFormEmail">
                    <Form.Label>{t('game.eula.form.email.label')}</Form.Label>
                    <Form.Control 
                        onChange={(event) => setEmail(event.target.value)}
                        type="email" 
                        placeholder={t('game.eula.form.email.placeholder')} 
                    />
                    <Form.Text className="text-muted">
                    {t('game.eula.form.email.description')}
                    </Form.Text>
                </Form.Group>
            </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleAccept}>{t('game.eula.button.accept')}</Button>
            </Modal.Footer>
        </Modal>
    );
};