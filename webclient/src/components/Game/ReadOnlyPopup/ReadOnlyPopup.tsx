import { useCallback, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { useTranslation } from "../../../i18n/i18n";

export const ReadOnlyPopup = () => {
    const [open, setOpen] = useState(true);
    const { t } = useTranslation();

    const onClose = useCallback(() => {
        setOpen(false);
    }, []);

    return (
        <Modal
            show={open}
            onHide={onClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
                <Modal.Title>{t('game.readonly.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {t('game.readonly.description')}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={onClose}>{t('game.readonly.button.close')}</Button>
            </Modal.Footer>
        </Modal>
    );
};