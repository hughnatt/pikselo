import { useCallback, useState } from "react";

import { Alert, Button, FormControl, InputGroup, Modal } from "react-bootstrap";

import { useTranslation } from "../../../i18n/i18n";

interface ISharePopupProps {
    open: boolean,
    onClose: () => void,
}
export const SharePopup = ({
    open,
    onClose,
}: ISharePopupProps) => {
    const { t } = useTranslation();
    const [showClipboardSuccess, setShowClipboardSuccess] = useState(false);

    const handleClipboard = useCallback(() => {
        setShowClipboardSuccess(true);
        navigator.clipboard.writeText(window.location.href);
    }, []);

    return (
        <Modal
            show={open}
            onHide={onClose}
            backdrop="static"
        >
            <Modal.Header>
                <Modal.Title>{t('game.share.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    {t('game.share.description')}
                </p>
                <InputGroup className="mb-3" >
                    <FormControl disabled placeholder={window.location.href}/>
                    <Button onClick={handleClipboard}>
                        <i className="fa fa-clipboard"></i>
                    </Button>
                </InputGroup>
                {
                    showClipboardSuccess &&
                        <Alert variant="success">{t('game.share.clipboard.success')}</Alert>
                }
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={onClose}>{t('game.share.button.close')}</Button>
            </Modal.Footer>
        </Modal>
    );
}