import { Spinner } from "react-bootstrap";
import './LoadingScreen.css';

export const LoadingScreen = () => (
    <div className="center-wrapper">
        <Spinner
            animation="grow"
            variant="light"
        />
    </div>
);