import { Modal } from "react-bootstrap";
import { useTranslation } from "../../../i18n/i18n";

export const ErrorScreen = () => {
    const { t } = useTranslation();

    return (
        <Modal
            show={true}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
            <Modal.Title>{t('game.error.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {t('game.error.description')}
            </Modal.Body>
        </Modal>
    )
};