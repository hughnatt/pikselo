import { useCallback, useState } from 'react';
import { Colors } from '../../../helpers/convertColor';
import './Palette.css';

interface IPaletteProps {
    onColorChange: (color: number) => void,
    hidden: boolean,
}

export const Palette = ({
    onColorChange,
    hidden,
}: IPaletteProps) => {
    const [selectedColorIndex, setSelectedColorIndex] = useState(0);

    const handleColorChange = useCallback((index) => {
        setSelectedColorIndex(index)
        onColorChange(index);
    }, [onColorChange]);

    return (
        <div className={`colors ${hidden ? 'colors-hidden' : ''}`}>
            {
                Colors.map((value, index) =>
                    <span
                        key={index}
                        onClick={() => handleColorChange(index)}
                        className={`color ${selectedColorIndex === index ? 'color-selected' : ''}`}
                        style={
                            {
                                backgroundColor: `rgb(${value.r}, ${value.g}, ${value.b})`,
                            }
                        }
                    />
                )
            }
        </div>
    );
};