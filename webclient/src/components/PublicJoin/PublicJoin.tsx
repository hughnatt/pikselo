import { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Redirect, useHistory, useRouteMatch } from "react-router-dom";
import { useJoinApi } from "../../data/useJoinApi";
import { useTranslation } from "../../i18n/i18n";

const PublicAuthDisabled = () => {
    const { t } = useTranslation();

    return (
        <Modal
            show={true}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header>
            <Modal.Title>{t('error.publicauthdisabled.title')}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {t('error.publicauthdisabled.description')}
            </Modal.Body>
        </Modal>
    );
};

export const PublicJoin = () => {
    const { publicJoin } = useJoinApi();
    const [token, setToken] = useState<string>();
    const [loading, setLoading] = useState<boolean>(false);
    const { path } = useRouteMatch();
    const history = useHistory();

    useEffect(() => {
        setLoading(true);
        publicJoin()
            .then((data) => {
                setToken(data.token);
            })
            .catch(() => {
                // Nothing here
            })
            .finally(() => {
                setLoading(false);
            })
    }, [publicJoin, history, path]);

    return (
        loading
            ? null
            : (
                token
                    ? <Redirect to={`/play/${token}`}/>
                    : <PublicAuthDisabled />
            )
    );
};