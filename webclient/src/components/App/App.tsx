import './App.css';

import { ToastContainer } from 'react-toastify';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { publicUrl } from "../../constants";
import { Admin } from "../Admin/Admin";
import { Game } from "../Game/Game";
import { PublicJoin } from '../PublicJoin/PublicJoin';
import { InvalidToken } from '../InvalidToken/InvalidToken';
import { Suspense } from 'react';
import { Spinner } from 'react-bootstrap';

export const App = () => (
    <Suspense fallback={<Spinner animation="grow" />}>
        <ToastContainer />
        <BrowserRouter basename={publicUrl}>
            <Switch>
                <Route path="/error/token">
                    <InvalidToken />
                </Route>
                <Route path="/play/:token">
                    <Game />
                </Route>
                <Route path="/admin">
                    <Admin />
                </Route>
                <Route exact path="/play">
                    <PublicJoin />
                </Route>
                <Route path="*">
                    <Redirect to="/play" />
                </Route>
            </Switch>
        </BrowserRouter>
    </Suspense>
);