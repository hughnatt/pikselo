const apiHost = process.env.REACT_APP_API_HOST;
const apiPort = process.env.REACT_APP_API_PORT;

const socketIOHost = process.env.REACT_APP_SOCKETIO_HOST ?? apiHost;
const socketIOPort = process.env.REACT_APP_SOCKETIO_PORT ?? apiPort;

export const apiBasePath = `${apiHost}:${apiPort}`;
export const socketIOPath = `${socketIOHost}:${socketIOPort}`;
export const publicUrl = process.env.PUBLIC_URL;
export const publicJoinUrl = `${apiBasePath}/api/v1/qrauth/join/public/redirect`;