import React, { Dispatch, useMemo, useReducer } from 'react';
import { useContext } from 'react';
import { Session } from '../types/Session';



type StoreData = {
    session?: Session,
}

const getInitialStoreData = (): StoreData => {
    const accessToken = localStorage.getItem('accessToken');
    const refreshToken = localStorage.getItem('refreshToken');
    let session: Session | undefined = undefined;


    if (accessToken && refreshToken) {
        session = {
            accessToken,
            refreshToken,
        };
    }

    return ({
        session,
    });
};


type Action =
| { type: 'UPDATE_SESSION', session?: Session }

interface IStoreContext {
    store: StoreData,
    dispatch: Dispatch<Action>,
}

export const StoreContext = React.createContext<IStoreContext>({ store: {}, dispatch: () => {}});

const storeReducer = (state: StoreData, action: Action): StoreData => {
    switch (action.type) {
        case 'UPDATE_SESSION':
            state.session = action.session;
            if (action.session) {
                localStorage.setItem('accessToken', action.session.accessToken);
                localStorage.setItem('refreshToken', action.session.refreshToken);
            } else {
                localStorage.removeItem('accessToken');
                localStorage.removeItem('refreshToken');
            }
            break;
    }
    return state;
}

export const StoreProvider: React.FC = ({ children }) => {
    const [store, dispatch] = useReducer(storeReducer, getInitialStoreData());

    const contextValue = useMemo(() => {
        return { store, dispatch };
    }, [store, dispatch]);

    return (
        <StoreContext.Provider value={contextValue}>
            {children}
        </StoreContext.Provider>
    );
};

export const useStore = () => useContext(StoreContext);
