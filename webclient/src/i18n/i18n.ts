import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { useTranslation } from 'react-i18next';

import HttpBackend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { apiBasePath } from '../constants';
// don't want to use this?
// have a look at the Quick start guide
// for passing in lng and translations on init

i18n
  .use(HttpBackend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    backend: {
        loadPath: `${apiBasePath}/static/assets/locales/{{lng}}/{{ns}}.json`,
    },

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    }
  });

export { useTranslation };
export default i18n;