import { SessionData } from '../types/data/SessionData';
import { useAdminApi } from './useAdminApi';

export const useAuthApi = () => {
    const { client } = useAdminApi();

    const loginWithPassword = (login: string, password: string) : Promise<SessionData> => (
        client.post<SessionData>('/admin/auth/login', { login, password })
            .then((response) => response.data)
    );

    return {
        loginWithPassword,
    }
};
