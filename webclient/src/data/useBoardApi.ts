import { useCallback } from 'react';
import { SinglePixelData } from '../types/data/SinglePixelData';
import { GetBoardDataModel } from '../types/GetBoardDataModel';
import { useAdminApi } from './useAdminApi';

export const useBoardApi = () => {
    const { client } = useAdminApi();

    const getBoard = useCallback(() : Promise<GetBoardDataModel> => (
        client
            .get<GetBoardDataModel>(`/board`)
            .then((response) => response.data)
    ), [client]);

    const cleanBoard = useCallback((): Promise<void> => (
        client
            .delete<void>('/board')
            .then((response) => response.data)
    ), [client]);

    const getPixel = useCallback((x: number, y: number): Promise<SinglePixelData> => (
        client
            .get<SinglePixelData>(`/board/pixel?x=${x}&y=${y}`)
            .then((response) => response.data)
    ), [client])

    return {
        cleanBoard,
        getBoard,
        getPixel,
    };
};
