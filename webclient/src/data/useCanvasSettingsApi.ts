import { useCallback } from 'react';
import { CanvasSettingsInfoData } from '../types/data/CanvasSettingsInfoData';
import { UpdateCanvasSettingsParameter } from '../types/data/params/UpdateCanvasSettingsParameter';
import { useAdminApi } from './useAdminApi';

export const useCanvasSettingsApi = () => {
    const { client } = useAdminApi();

    const getCanvasSettings = useCallback(() => (
        client
            .get<CanvasSettingsInfoData>('/admin/settings/canvas')
            .then((response) => response.data)
    ), [client]);

    const updateCanvasSettings = useCallback((canvasSettings: UpdateCanvasSettingsParameter) => (
        client
            .put<CanvasSettingsInfoData>('/admin/settings/canvas', canvasSettings)
            .then((response) => response.data)
    ), [client]);

    return {
        getCanvasSettings,
        updateCanvasSettings,
    };
};
