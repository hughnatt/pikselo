import { useCallback } from 'react';
import { useAdminApi } from './useAdminApi';

export const useUsersApi = () => {
    const { client } = useAdminApi();

    const banUser = useCallback((userId: string) => (
        client
            .post<void>(`/user/ban/${userId}`)
            .then((response) => response.data)
    ), [client]);

    return {
        banUser,
    };
};
