import axios from "axios";
import { useCallback } from "react";
import { apiBasePath } from "../constants";
import { TokenData } from "../types/data/TokenData";

const client = axios.create({
    baseURL: `${apiBasePath}/api/v1`,
});

export const useJoinApi = () => {

    const publicJoin = useCallback((): Promise<TokenData> => (
        client.get<TokenData>('/qrauth/join/public')
            .then((response) => response.data)
    ), []);

    return {
        publicJoin,
    };
};