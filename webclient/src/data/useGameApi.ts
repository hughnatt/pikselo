import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { useCallback, useEffect } from 'react';
import { useHistory } from "react-router-dom";

import { apiBasePath } from "../constants";
import { DrawPixelDataModel } from '../types/DrawPixelDataModel';
import { GetBoardDataModel } from "../types/GetBoardDataModel";
import { UserStatus } from "../types/UserStatus";


export const client = axios.create({
    baseURL: `${apiBasePath}/api/v1`,
});

export const useGameApi = (token: string) => {
    const history = useHistory();

    const requestInterceptor = useCallback(() => ({
        request: (config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${token}`,
            };
            return config;
        },
        error: (error: AxiosError) => Promise.reject(error),
    }), [token]);

    const responseInterceptor = useCallback(() => ({
        response: (response: AxiosResponse) => response,
        error: (error: AxiosError) => {
            if (error.response && error.response.status === 401) {
                history.push('/error/token');
            }
            return Promise.reject(error);
        },
    }), [history]);

    useEffect(() => {
        const clientRequestInterceptor = client.interceptors.request.use(
            requestInterceptor().request,
            requestInterceptor().error,
        );

        return () => {
            client.interceptors.request.eject(clientRequestInterceptor)
        };
    }, [requestInterceptor]);

    useEffect(() => {
        const clientResponseInterceptor = client.interceptors.response.use(
            responseInterceptor().response,
            responseInterceptor().error,
        );

        return () => {
            client.interceptors.response.eject(clientResponseInterceptor);
        }
    }, [responseInterceptor]);

    const getBoard = useCallback(() : Promise<GetBoardDataModel> => (
        client
            .get<GetBoardDataModel>(`/board`)
            .then((response) => response.data)
    ), []);

    const drawPixel = useCallback((x: number, y: number, color: number) : Promise<DrawPixelDataModel> => (
        client
            .put<DrawPixelDataModel>(`/board/pixel?x=${x}&y=${y}&c=${color}`)
            .then((response) => response.data)
    ), []);

    const getStatus = useCallback((): Promise<UserStatus> => (
        client
            .get<UserStatus>('/user/status')
            .then((response) => response.data)
    ), []);

    const acceptEula = useCallback((email?: string): Promise<void> => (
        client
            .put<void>('/user/eula', { email })
            .then((response) => response.data)
    ), []);

    return {
        getBoard,
        drawPixel,
        getStatus,
        acceptEula,
    };
};
