import axios from 'axios';
import { useEffect } from 'react';
import { apiBasePath } from '../constants';
import { useStore } from '../contexts/Store';
import { SessionData } from '../types/data/SessionData';

const clientOptions = {
    baseURL: `${apiBasePath}/api/v1`,
}

const client = axios.create(clientOptions);

export const useAdminApi = () => {
    const { store, dispatch } = useStore();

    useEffect(() => {
        const requestInterceptor = client.interceptors.request.use(
            (config) => {
                const token = store.session?.accessToken;

                if (token) {
                    config.headers.Authorization = `Bearer ${token}`
                }

                return config;
            },
            (error) => Promise.reject(error),
        );

        return () => {
            client.interceptors.request.eject(requestInterceptor);
        };
    }, [store.session?.accessToken]);

    useEffect(() => {
        const responseInterceptor = client.interceptors.response.use(
            (response) => response,
            async (error) => {
                const originalRequest = error.config;

                if (error.response?.status === 401) {
                    try {
                        const response = await axios.create(clientOptions).post<SessionData>(
                            '/admin/auth/refresh',
                            {
                                refreshToken: store.session?.refreshToken,
                            }
                        )

                        if (response.status === 200) {
                            dispatch({ type: 'UPDATE_SESSION', session: response.data });

                            axios.defaults.headers.common.Authorization = `Bearer ${response.data.accessToken}`;

                            return axios(originalRequest);
                        }

                        throw new Error('Access token refresh failed');
                    } catch (error) {
                        dispatch({ type: 'UPDATE_SESSION', session: undefined})
                        return Promise.reject(error);
                    }
                }
            }
        );

        return () => {
            client.interceptors.response.eject(responseInterceptor);
        };
    }, [dispatch, store.session?.refreshToken]);

    return { client };
};