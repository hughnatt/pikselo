import { useCallback } from 'react';
import { AuthSettingsInfoData } from '../types/data/AuthSettingsInfoData';
import { UpdateAuthSettingsParameter } from '../types/data/params/UpdateAuthSettingsParameter';
import { useAdminApi } from './useAdminApi';

export const useAuthSettingsApi = () => {
    const { client } = useAdminApi();

    const getAuthSettings = useCallback((): Promise<AuthSettingsInfoData> => (
        client
            .get<AuthSettingsInfoData>('/admin/settings/auth')
            .then((response) => response.data)
    ), [client]);

    const updateAuthSettings = useCallback((authSettings: UpdateAuthSettingsParameter) : Promise<AuthSettingsInfoData> => (
        client
            .put<AuthSettingsInfoData>('/admin/settings/auth', authSettings)
            .then((response) => response.data)
    ), [client]);

    return {
        getAuthSettings,
        updateAuthSettings,
    };
};