import { useCallback } from 'react';
import { DashboardInfoData } from '../types/data/DashboardInfoData';
import { useAdminApi } from './useAdminApi';

export const useDashboardApi = () => {
    const { client } = useAdminApi();

    const getDashboard = useCallback(() => (
        client.get<DashboardInfoData>('/admin/dashboard')
            .then((response) => response.data)
    ), [client]);

    return {
        getDashboard,
    };
};
