import { RefObject, useEffect, useState } from 'react';

export const useInternalCanvas = (
    userCanvasRef: RefObject<HTMLCanvasElement>,
    internalCanvasRef: RefObject<HTMLCanvasElement>,
) => {
    const [internalCanvas, setInternalCanvas] = useState<HTMLCanvasElement | null>(null);
    const [userCanvas, setUserCanvas] = useState<HTMLCanvasElement | null>(null);
    const [internalContext, setInternalContext] = useState<CanvasRenderingContext2D | null>();
    const [userContext, setUserContext] = useState<CanvasRenderingContext2D | null>();

    useEffect(() => {
        const internalCanvas = internalCanvasRef.current;
        const userCanvas = userCanvasRef.current;
        setInternalCanvas(internalCanvas);
        setUserCanvas(userCanvas);
        setInternalContext(internalCanvas?.getContext('2d'));
        setUserContext(userCanvas?.getContext('2d'));
    }, [userCanvasRef, internalCanvasRef]);

    return {
        internalCanvas,
        userCanvas,
        internalContext,
        userContext,
    }
};
