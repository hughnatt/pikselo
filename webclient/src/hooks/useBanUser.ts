import { useCallback, useState } from "react";
import { useUsersApi } from "../data/useUsersApi";
import { useNotifier } from "./useNotifier";

export const useBanUser = () => {
    const { banUser: banUserApi } = useUsersApi();
    const [isLoading, setIsLoading] = useState(false);
    const { postError } = useNotifier();

    const banUser = useCallback((userId: string) => {
        setIsLoading(true);
        banUserApi(userId)
            .catch((error) => {
                postError('Failed to ban user', error);
            })
            .finally(() => {
                setIsLoading(false);
            })
    }, [banUserApi, postError]);

    return {
        isLoading,
        banUser,
    }
};