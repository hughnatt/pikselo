import { useCallback, useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import { socketIOPath } from '../constants';
import { convertColor } from '../helpers/convertColor';

export const socket = io(socketIOPath);

export const usePixelListener = (boardWidth: number, imageData?: ImageData) => {
    const [refresh, toggleRefresh] = useState(true);
    const [dirtyPixels, setDirtyPixels] = useState<{
        x: number,
        y: number,
        c: number,
    }[]>([]);

    const drawPixel = useCallback((pixel: { x: number, y: number, c: number }) => {
        const offset = (pixel.x + pixel.y * boardWidth) * 4;
        const color = convertColor(pixel.c);

        if (imageData) {
            imageData.data[offset + 0] = color.r;
            imageData.data[offset + 1] = color.g;
            imageData.data[offset + 2] = color.b;
            imageData.data[offset + 3] = 255;
        }
    }, [imageData, boardWidth]);

    const drawDirtyPixels = useCallback(() => {
        if (dirtyPixels.length > 0) {
            dirtyPixels.forEach((dirtyPixel) => drawPixel(dirtyPixel));
        }
    }, [dirtyPixels, drawPixel]);

    const updatePixel = useCallback((data) => {
        if (imageData) {
            drawPixel({
                x: Number(data.x),
                y: Number(data.y),
                c: Number(data.c),
            });
            drawDirtyPixels();
        } else {
            setDirtyPixels((dirtyPixels) => ([
                ...dirtyPixels,
                {
                    x: data.x,
                    y: data.y,
                    c: data.c,
                },
            ]));
        }
        toggleRefresh((refresh) => !refresh);
    }, [imageData, drawDirtyPixels, drawPixel]);

    useEffect(() => {
        socket.on('update-pixel', updatePixel);

        return () => {
            socket.off('update-pixel');
        }
    }, [updatePixel]);

    return {
        refresh,
    }
};

