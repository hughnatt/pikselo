import { useCallback, useEffect, useState } from 'react';

import 'hammerjs';
import { useViewport } from './useViewport';

export const useHammer = ({
    userCanvas,
    windowHeight,
    windowWidth,
    canvasHeight,
    canvasWidth,
    onPixelClick,
    toggleRecenter,
}:{
    userCanvas: HTMLCanvasElement | null,
    windowHeight: number,
    windowWidth: number,
    canvasHeight: number,
    canvasWidth: number,
    onPixelClick: (x: number, y: number) => void,
    toggleRecenter: unknown,
}) => {
    const [canvasScale, setCanvasScale] = useState(1);
    const [lastScale, setLastScale] = useState(1);
    const [canvasOffset, setCanvasOffset] = useState({ x: 0, y: 0 });
    const [previousPan, setPreviousPan] = useState({ x: 0, y: 0 });
    const [hammerManager, setHammerManager] = useState<HammerManager>();
    const { viewport } = useViewport({
        canvasWidth,
        canvasHeight,
        windowWidth,
        windowHeight,
        canvasScale,
        canvasOffset,
    });

    const handlePanMove = useCallback((event: HammerInput) => {
        setCanvasOffset((canvasOffset) => {
            const panMovement = {
                x: event.deltaX - previousPan.x,
                y: event.deltaY - previousPan.y
            };

            canvasOffset.x -= (panMovement.x / windowWidth) * viewport.width;
            canvasOffset.y -= (panMovement.y / windowHeight) * viewport.height;
            setPreviousPan(() => ({x: event.deltaX, y: event.deltaY}));

            return { x: canvasOffset.x, y: canvasOffset.y }
        });
    }, [previousPan, viewport, windowHeight, windowWidth]);

    const handlePanStart = useCallback((event: HammerInput) => {
        setCanvasOffset((canvasOffset) => {
            const panMovement = {
                x: event.deltaX,
                y: event.deltaY
            }

            canvasOffset.x -= (panMovement.x / windowWidth) * viewport.width;
            canvasOffset.y -= (panMovement.y / windowHeight) * viewport.height;
            setPreviousPan(() => ({ x: 0, y: 0}));

            return { x: canvasOffset.x, y: canvasOffset.y };
        });
    }, [viewport, windowHeight, windowWidth]);

    const setupTap = useCallback(() => {
        hammerManager?.off('tap');
        hammerManager?.on('tap', (event: HammerInput) => {
            const tapX = event.center.x;
            const tapY = event.center.y;
            const pixelX = Math.floor((tapX / windowWidth) * viewport.width + viewport.x)
            const pixelY = Math.floor((tapY / windowHeight) * viewport.height + viewport.y)

            if (pixelX >= 0 && pixelX < canvasWidth && pixelY >= 0 && pixelY < canvasHeight) {
                onPixelClick(pixelX, pixelY);
            }
        })
    }, [
        hammerManager,
        canvasHeight,
        canvasWidth,
        windowHeight,
        windowWidth,
        viewport,
        onPixelClick,
    ]);

    const setupPan = useCallback(() => {
        hammerManager?.off('panstart');
        hammerManager?.on('panstart', handlePanStart);
        hammerManager?.off('panmove');
        hammerManager?.on('panmove', handlePanMove);
    }, [hammerManager, handlePanMove, handlePanStart]);

    const setupZoom = useCallback((canvas: HTMLCanvasElement) => {
        hammerManager?.off('pinch');
        hammerManager?.on('pinch', (event: HammerInput) => {
            setCanvasScale(() => Math.max(.1, Math.min(lastScale / (event.scale), 2)));
        });
        hammerManager?.off('pinchend');
        hammerManager?.on('pinchend', (_event: HammerInput) => {
            setLastScale(() => canvasScale);
        });

        canvas.onwheel = (event) => {
            event.preventDefault();
            setCanvasScale((canvasScale) => {
                canvasScale += event.deltaY * 0.001;
                canvasScale = Math.min(Math.max(.1, canvasScale), 2);
                return canvasScale;
            });

        }
    }, [hammerManager, canvasScale, lastScale]);

    const setupHammer = useCallback((canvas: HTMLCanvasElement) => {
        const hammertime = new Hammer(canvas);
        hammertime.get('pinch').set({ enable: true });
        hammertime.get('pan').set({ direction: Hammer.DIRECTION_ALL });
        setHammerManager(hammertime);
    }, []);

    useEffect(() => {
        if (userCanvas) {
            setupHammer(userCanvas);
        }
    }, [userCanvas, setupHammer]);

    useEffect(() => {
        if (userCanvas) {
            setupZoom(userCanvas);
        }
    }, [userCanvas, setupZoom]);

    useEffect(() => {
        setupTap();
    }, [setupTap]);

    useEffect(() => {
        setCanvasScale(1);
        setLastScale(1);
        setCanvasOffset({ x: 0, y: 0 });
        setPreviousPan({ x: 0, y: 0 });
    }, [toggleRecenter]);

    useEffect(() => {
        setupPan();
    }, [setupPan]);

    return {
        canvasScale,
        canvasOffset,
    };
};
