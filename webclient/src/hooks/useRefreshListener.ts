import { useEffect, useState } from "react";
import { io } from "socket.io-client";
import { socketIOPath } from "../constants";

export const socket = io(socketIOPath);

export const useRefreshListener = () => {
    const [refresh, setRefresh] = useState(Date.now());

    useEffect(() => {
        socket.on('refresh-board', () => {
            setRefresh(Date.now());
        });

        return () => {
            socket.off('refresh-board');
        };
    }, []);

    return {
        refresh,
    };
};