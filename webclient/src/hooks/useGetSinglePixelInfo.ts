import { useEffect, useState } from "react";
import { useBoardApi } from "../data/useBoardApi";

import { SinglePixelInfo } from "../types/SinglePixelInfo";
import { useNotifier } from "./useNotifier";

interface IGetSinglePixelInfoHookProps {
    position?: {
        x: number,
        y: number,
    }
}

export const useGetSinglePixelInfo = ({
    position,
}: IGetSinglePixelInfoHookProps) => {
    const [isLoading, setIsLoading] = useState(false);
    const [singlePixelInfo, setSinglePixelInfo] = useState<SinglePixelInfo>();
    const { getPixel } = useBoardApi();
    const { postError } = useNotifier();

    useEffect(() => {
        if (position) {
            setIsLoading(true);
            getPixel(position.x, position.y)
                .then((singlePixelData) => {
                    setSinglePixelInfo(singlePixelData);
                })
                .catch((error) => {
                    postError('Failed to get pixel data', error);
                })
                .finally(() => {
                    setIsLoading(false);
                })
        }
    }, [position, getPixel, postError]);

    return {
        isLoading,
        singlePixelInfo,
    };
};