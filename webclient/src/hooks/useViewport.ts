import { useEffect, useState } from 'react';
import { getViewport } from '../helpers/getViewport';

interface IViewport {
    x: number,
    y: number,
    width: number,
    height: number,
}

export const useViewport = ({
    canvasWidth,
    canvasHeight,
    windowWidth,
    windowHeight,
    canvasScale,
    canvasOffset,
}:{
    canvasWidth: number,
    canvasHeight: number,
    windowWidth: number,
    windowHeight: number,
    canvasScale: number,
    canvasOffset: {x: number, y: number},
}) => {
    const [viewport, setViewport] = useState<IViewport>(getViewport(
        canvasWidth,
        canvasHeight,
        windowWidth,
        windowHeight,
        canvasScale,
        canvasOffset,
    ));

    useEffect(() => {
        setViewport(getViewport(
            canvasWidth,
            canvasHeight,
            windowWidth,
            windowHeight,
            canvasScale,
            canvasOffset,
        ));
    }, [canvasWidth, canvasHeight, windowWidth, windowHeight, canvasScale, canvasOffset]);

    return {
        viewport,
    }
};
