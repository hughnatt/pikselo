import { toast, ToastOptions } from 'react-toastify';
import { useCallback } from 'react';

const toastifyOptions: ToastOptions = {
    theme: 'colored',
    position: toast.POSITION.TOP_LEFT,
    toastId: 'defaultId',
};

export const useNotifier = () => {
    const postInfo = useCallback((message: string) => {
        toast.info(message, toastifyOptions);
    }, []);

    const postError = useCallback((message: string, error?: Error) => {
        if (error) {
            console.log(error);
        }
        toast.error(message, toastifyOptions);
    }, []);

    const postWarning = useCallback((message: string) => {
        toast.warn(message, toastifyOptions);
    }, []);

    return {
        postInfo,
        postError,
        postWarning,
    };
};
