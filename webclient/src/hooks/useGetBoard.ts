import { useCallback } from 'react';
import { useBoardApi } from '../data/useBoardApi';
import { useGameApi } from '../data/useGameApi';
import { convertColor } from '../helpers/convertColor';

export const useGetBoard = (token: string, admin: boolean = false) => {
    const { getBoard: getBoardApi } = useGameApi(token);
    const { getBoard: getBoardAdminApi} = useBoardApi();

    const getBoard = useCallback((internalContext?: CanvasRenderingContext2D | null) => {
        let getBoardPromise;
        if (admin) {
            getBoardPromise = getBoardAdminApi();
        } else {
            getBoardPromise = getBoardApi();
        }

        return getBoardPromise
            .then((getBoardDataModel) => {
                const boardBase64 = getBoardDataModel.data;
                const boardWidth = getBoardDataModel.width;
                const boardHeight = getBoardDataModel.height;
                const maxWidth = getBoardDataModel.maxWidth;

                const board = Uint8Array.from(atob(boardBase64), c => c.charCodeAt(0));

                const colorBoard = [];

                let k = 0;
                let offset = 0;
                for (let i = 0; i < boardHeight; i++) {
                    for (let j = 0; j < boardWidth / 2; j++) {
                        colorBoard[k + 0] = (board[offset + j] >> 4) & 0xF;
                        colorBoard[k + 1] = (board[offset + j] >> 0) & 0xF;
                        k += 2;
                    }
                    offset += (maxWidth) / 2;
                }

                if (!internalContext) {
                    throw new Error('Unexpected empty internal context');
                }

                const internalImageData = internalContext.createImageData(boardWidth, boardHeight)
                for (let i = 0; i < boardWidth * boardHeight; i++) {
                    const j = i * 4;
                    const hexaColor = convertColor(colorBoard[i]);
                    internalImageData.data[j + 0] = hexaColor.r;
                    internalImageData.data[j + 1] = hexaColor.g;
                    internalImageData.data[j + 2] = hexaColor.b;
                    internalImageData.data[j + 3] = 0xFF;
                }

                return {
                    height: boardHeight,
                    width: boardWidth,
                    imageData: internalImageData,
                    readOnly: getBoardDataModel.readOnly,
                };
            })
    }, [getBoardApi, admin, getBoardAdminApi]);

    return {
        getBoard,
    };
};
