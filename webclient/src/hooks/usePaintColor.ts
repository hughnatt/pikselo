import { useCallback } from 'react';
import { useGameApi } from '../data/useGameApi';

export const usePaintColor = (
    token: string,
    color: number,
    setCooldown: (delay: number) => void,
    disabled: boolean,
) => {
    const { drawPixel } = useGameApi(token);

    const paintColor = useCallback((x: number, y: number) => {
        if (disabled) { return; }
        drawPixel(x, y, color)
            .then((drawPixelDataModel) => {
                setCooldown(0);
                setCooldown(drawPixelDataModel.cooldown);
            })
            .catch((error) => {
                setCooldown(0);
                if (error.response?.data?.data?.cooldown) {
                    setCooldown(error.response.data.data.cooldown)
                }
            })
    }, [color, drawPixel, setCooldown, disabled]);

    return {
        paintColor,

    }
};
