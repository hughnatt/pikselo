export type CanvasSettingsInfo = {
    boardHeight: number,
    boardWidth: number,
    cooldown: number,
    readOnly: boolean,
}
