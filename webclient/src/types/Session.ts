export type Session = {
    refreshToken: string,
    accessToken: string,
}
