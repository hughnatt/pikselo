export enum GameStatus {
    LOADING,
    CLOSED,
    READY,
    ERROR,
};