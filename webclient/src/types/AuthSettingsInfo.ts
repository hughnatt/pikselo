export interface AuthSettingsInfo {
    publicAuthEnabled: boolean,
    qrAuthEnabled: boolean,
}
