export type GetBoardDataModel = {
    height: number,
    width: number,
    maxWidth: number,
    maxHeight: number,
    data: string,
    readOnly: boolean,
};
