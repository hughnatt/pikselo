export type SinglePixelData = {
    x: number,
    y: number,
    c: number,
    userId: string | undefined,
    lastDrawTime: number | undefined,
};