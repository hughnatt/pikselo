export type UpdateCanvasSettingsParameter = Partial<{
    boardWidth: number,
    boardHeight: number,
    cooldown: number,
}>
