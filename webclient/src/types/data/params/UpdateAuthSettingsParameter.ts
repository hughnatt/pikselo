export type UpdateAuthSettingsParameter = Partial<{
    publicAuthEnabled: boolean,
    qrAuthEnabled: boolean,
}>
