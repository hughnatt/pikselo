export type CanvasSettingsInfoData = {
    canvasSettings: {
        boardHeight: number,
        boardWidth: number,
        cooldown: number,
        readOnly: boolean,
    }
}
