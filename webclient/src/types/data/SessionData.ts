export type SessionData = {
    accessToken: string,
    refreshToken: string,
}
