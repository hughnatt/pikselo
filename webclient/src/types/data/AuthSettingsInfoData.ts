export type AuthSettingsInfoData = {
    authSettings: {
        publicAuthEnabled: boolean,
        qrAuthEnabled: boolean,
    }
}
