export const getViewport = (
    canvasWidth: number,
    canvasHeight: number,
    windowWidth: number,
    windowHeight: number,
    canvasScale: number,
    // panMoves: {x: number, y: number}[],
    canvasOffset: {x: number, y:number},
) => {

    const center = {
        x: canvasWidth / 2,
        y: canvasHeight / 2
    }

    let viewportWidth = 0
    let viewportHeight = 0
    let viewportX = 0
    let viewportY = 0

    if (windowWidth > windowHeight) {
        viewportHeight = canvasHeight
        viewportWidth = canvasWidth * windowWidth/windowHeight
    } else {
        viewportWidth = canvasWidth
        viewportHeight = canvasHeight * windowHeight/windowWidth
    }

    // Apply scale
    viewportWidth *= canvasScale
    viewportHeight *= canvasScale

    // for (let i = 0; i < panMoves.length; i++)
    // {
    //     const panMove = panMoves[i]
    //     // Add panMovement to offset (if any)
    //     canvasOffset.x -= (panMove.x / windowWidth) * viewportWidth
    //     canvasOffset.y -= (panMove.y / windowHeight) * viewportHeight
    // }
    // panMoves = [];

    // Apply offset
    viewportX = center.x - (viewportWidth/2) + canvasOffset.x
    viewportY = center.y - (viewportHeight/2) + canvasOffset.y

    return {
        x: viewportX,
        y: viewportY,
        width: viewportWidth,
        height: viewportHeight,
    }
};
