# PIKSELO
A Collaborative Pixel Canvas Experience

## What is Pikselo ?

Pikselo is an implementation of a collaborative pixel canvas. Freely inspired by Reddit r/place experience that occured on April 1st, 2017, it was presented during the Devfest Nantes 2019 at the SOLENT stand.

Pikselo was developped by me at SOLENT, and has since been released under a free and open-source licence.

[SOLENT is hiring!](https://www.linkedin.com/company/solent-sas/)

## Licensing

Pikselo source code has been released under a MIT Licence, a copy of which is distributed with the rest of the code. The brand and logo included in this project stays the property of their respectful owner.

## Contributing

I do not plan of continuing this project. Merge requests and issues will not be treated at the time I wrote this. Feel free to fork the project and improve it as you wish =)

## Production

The project consists of 2 part: an NodeJS/Express API and a react webclient.

2 Docker images are available for easier deployment. Feel free to deploy it any other way you need.

### API

The API only relies on a redis database.

```
version: '3'
services:
    pikselo-api:
     image: registry.gitlab.com/hughnatt/pikselo/server:latest
     depends_on:
       - redis
     restart: always
     environment:
       - REDIS_HOST=redis
       - REDIS_PORT=6379
       - BOARD_WIDTH=100
       - BOARD_HEIGHT=100
       - JWT_SECRET=YOUR_SECRET_HERE
       - REDIRECT_URL=https://redirect.example.com/play
       - COOLDOWN=10
       - HOST=https://api.example.com
       - ADMIN_LOGIN=YOUR_ADMIN_ACCOUNT_LOGIN
       - ADMIN_PASSWORD=YOUR_ADMIN_ACCOUNT_PASSWORD
   redis:
     image: redis
     restart: always
     volumes:
       - ./redis_data:/data
```

### WEBCLIENT
The Docker container serves the webapp with nginx

```
version: '3'
services:
   pikselo-webclient:
     image: registry.gitlab.com/pikselo/webclient:latest
     restart: always
```
