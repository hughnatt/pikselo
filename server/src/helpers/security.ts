import { Request, Response, NextFunction, RequestHandler } from 'express';
import jwtAuthent from 'express-jwt';
import { sign as createJWT, verify as decodeJWT } from 'jsonwebtoken';
import { Socket } from 'socket.io';
import { UserRepository } from '../data/repository/UserRepository';
import { InvalidRefreshTokenError } from '../models/exceptions/InvalidRefreshTokenError';

import { InvalidRolePermissionError } from '../models/exceptions/InvalidRolePermissionError';
import { MissingAuthorizationTokenError } from '../models/exceptions/MissingAuthorizationTokenError';
import { MissingRolePermissionError } from '../models/exceptions/MissingRolePermissionError';
import { UserRevokedPermissionError } from '../models/exceptions/UserRevokedPermissionError';
import { Role } from '../models/security/Role';
import { JWT } from '../models/services/JWT';
import { Session } from '../models/services/Session';
import { ADMIN_LOGIN, ADMIN_PASSWORD, JWT_SECRET } from '../util/constants';
import { Logger } from '../util/Logger';

const authentication = jwtAuthent({
    secret: JWT_SECRET,
    algorithms: ['HS256'],
    credentialsRequired: false,
    resultProperty: 'locals.user',
});

export const authorize = ((expectedRole: Role): RequestHandler[] => {
    return [
        authentication,
        async (_req: Request, res: Response, next: NextFunction) => {
            if (!res.locals.user) {
                next(new MissingAuthorizationTokenError());
            }

            const { id, role } = res.locals.user;

            if (!role) {
                next(new MissingRolePermissionError());
            }

            if (role !== expectedRole && role !== Role.ADMIN) {
                next(new InvalidRolePermissionError());
            }

            if (await UserRepository.isUserBan(id)) {
                next(new UserRevokedPermissionError());
            }

            res.locals.userId = id;
            res.locals.userRole = role;

            return next();
        },
    ];
});

export const authorizeSocket = ((expectedRole: Role): ((socket: Socket, next: (error?: Error) => void) => void) => (
    (socket, next) => {
        const token = socket.handshake.auth.token;
        if (!token) {
            Logger.info('Unauthorized socket connection');
            return next(new MissingRolePermissionError());
        }

        try {
            const data = validateToken(token);
            if (data.role !== expectedRole) {
                Logger.info('Unauthorized socket connection');
                return next(new InvalidRolePermissionError());
            }
        } catch (error) {
            return next(error as Error);
        }

        return next();
    })
);

export const generateSessionForRole = (id: string, role: Role): Session => {
    const accessToken = createJWT({
        id,
        role,
    }, JWT_SECRET, { expiresIn: '1h'});

    const refreshToken = createJWT({
        id,
        role,
    }, JWT_SECRET, { expiresIn: '72h' });

    return {
        accessToken,
        refreshToken,
    };
};

export const getAccessTokenFromRefreshToken = (refreshToken: string): Promise<JWT> => {
    try {
        const { id, role } = decodeJWT(refreshToken, JWT_SECRET) as { id: string, role: Role };
        const accessToken = createJWT({
            id,
            role,
        }, JWT_SECRET);
        return Promise.resolve(accessToken);
    } catch (error) {
        return Promise.reject(new InvalidRefreshTokenError());
    }
};

export const createDefaultAdminAccount = (): Promise<void> => {
    Logger.info('Creating default admin account');

    return UserRepository.createUser(ADMIN_LOGIN, Role.ADMIN).then(() =>
        UserRepository.changePassword(ADMIN_LOGIN, ADMIN_PASSWORD)
    );
};

export const validateToken = (token: JWT): Record<string, unknown> => {
    return decodeJWT(token, JWT_SECRET) as { id: string, role: Role };
};
