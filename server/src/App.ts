import './env';

import cors from 'cors';
import express, { NextFunction, Request, Response } from 'express';
import expressWinston from 'express-winston';
import { createServer } from 'http';
import winston from 'winston';
import path from 'path';

import { RedisClient } from './data/RedisClient';
import { SocketServer } from './data/SocketServer';
import { canvasRouter } from './routers/CanvasRouter';
import { qrAuthRouter } from './routers/QRAuthRouter';
import { swaggerRouter } from './routers/SwaggerRouter';
import { Logger } from './util/Logger';
import { PORT } from './util/constants';
import { authorize, createDefaultAdminAccount } from './helpers/security';
import { Role } from './models/security/Role';
import { AppError } from './models/exceptions/AppError';
import { adminRouter } from './routers/AdminRouter';
import { QRAuthService } from './services/QRAuthService';
import { userRouter } from './routers/UserRouter';

const expressApp = express();
const httpServer = createServer(expressApp);
SocketServer.setupSocketServer(httpServer);
RedisClient.setupRedisClient();
Logger.setupLogger();

expressApp.use(cors());
expressApp.use(express.json());

expressApp.get('/', authorize(Role.ADMIN), (_req: Request, res: Response) => {
    res.status(200).send('Hello World!');
});

// Logs HTTP request.
expressApp.use(expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: Logger.logsFormat,
    meta: true,
    expressFormat: true,
    colorize: true,
    level: 'info',
}));

// Logs express errors.
expressApp.use(expressWinston.errorLogger({
    transports: [new winston.transports.Console()],
    format: winston.format.simple(),
}));

expressApp.use('/api/v1/', canvasRouter);
expressApp.use('/api/v1/qrauth', qrAuthRouter);
expressApp.use('/api/v1/admin', adminRouter);
expressApp.use('/api/v1/user', userRouter);
expressApp.use('/swagger', swaggerRouter);

// Error middleware.
// NB : "_next" parameter must be declared or Express won't see this middleware
// as an error middleware.
// eslint-disable-next-line @typescript-eslint/no-unused-vars
expressApp.use((error: AppError, _req: Request, res: Response, _next: NextFunction) => {
    Logger.error(error.message);
    res.status(error.status ?? 500);
    res.json({
        status: error.status ?? 500,
        message: error.message,
        data: error.data,
    });
});

expressApp.use('/static', express.static(path.join(__dirname, '../public')));

createDefaultAdminAccount();
QRAuthService.setupQRAuth();

httpServer.listen(PORT, () => {
    Logger.info(`Server Started at port ${PORT}`);
});
