import { Server as HttpServer} from 'http';
import { Server } from 'socket.io';
import { authorizeSocket } from '../helpers/security';

import { SocketServerNotInitializedError } from '../models/exceptions/SocketServerNotInitializedError';
import { Role } from '../models/security/Role';

export const SocketServer = (() => {
    let socketServer: Server;

    const setupSocketServer = (httpServer: HttpServer) => {
        socketServer = new Server();
        socketServer.attach(httpServer, {
            cors: {
                origin: '*',
                methods: ['GET', 'POST']
            }
        });
        socketServer.of('/qrauth').use(authorizeSocket(Role.ADMIN));
        // socketServer.of('/').use(authorizeSocket(Role.USER));
    };

    return {
        setupSocketServer,
        getInstance: () => {
            if (!socketServer) {
                throw new SocketServerNotInitializedError();
            }
            return socketServer;
        },
    };
})();

