import redis from 'redis';
import { GenericRedisError } from '../models/exceptions/GenericRedisError';

import { RedisClientNotInitializedError } from '../models/exceptions/RedisClientNotInitializedError';
import { REDIS_HOST, REDIS_PORT } from '../util/constants';
import { Logger } from '../util/Logger';

export const RedisClient = (() => {
    let redisClient: redis.RedisClient;

    const setupRedisClient = () => {
        redisClient = redis.createClient({
            host: REDIS_HOST,
            port: REDIS_PORT,
            return_buffers: true,
        });

        redisClient.on('error', (error) => {
            Logger.error('Failed to connect to the redis server : ', error);
            process.exit(-1);
        });

        redisClient.on('connect', () => {
            Logger.info(`Connected to redis database at ${REDIS_HOST}:${REDIS_PORT}`);
        });
    };

    const hset = (key: string, set: string, value: string): Promise<number> => (
        new Promise((resolve, reject) =>
            getInstance().hset(key, set, value, (error, data) => (
                error ? reject(new GenericRedisError()) : resolve(data)
            ))
        )
    );

    const hget = (key: string, set: string): Promise<string | undefined> => (
        new Promise((resolve, reject) =>
            getInstance().hget(key, set, (error, data) => {
                if (error) {
                    reject(new GenericRedisError());
                } else {
                    if (data) {
                        resolve(data.toString());
                    } else {
                        resolve(undefined);
                    }
                }
            })
        )
    );

    const hincrby = (key: string, field: string, increment: number): Promise<number> => (
        new Promise((resolve, reject) => 
            getInstance().hincrby(key, field, increment, (error, data) => {
                if (error) {
                    reject(new GenericRedisError());
                } else {
                    resolve(data)
                }
            })
        )
    );

    const bitfield = (key: string, args: (string | number)[]): Promise<number[]> => (
        new Promise((resolve, reject) =>
            getInstance().bitfield(key, ...args, (error, data) => (
                error ? reject(new GenericRedisError()) : resolve(data)
            ))
        )
    );

    const sadd = (key: string, value: string): Promise<number> => (
        new Promise((resolve, reject) =>
            getInstance().sadd(key, value, (error, data) => (
                error ? reject(new GenericRedisError()) : resolve(data)
            ))
        )
    );

    const scard = (key: string): Promise<number> => (
        new Promise((resolve, reject) =>
            getInstance().scard(key, (error, data) => (
                error ? reject(new GenericRedisError()) : resolve(data)
            ))
        )
    );

    const set = (key: string, value: string): Promise<void> => (
        new Promise((resolve, reject) =>
            getInstance().set(key, value, (error) => (
                error ? reject(new GenericRedisError()) : resolve()
            ))
        )
    );

    const get = (key: string): Promise<string | undefined> => (
        new Promise((resolve, reject) =>
            getInstance().get(key, (error, data) => (
                error ? reject(new GenericRedisError()) : resolve(data ? data.toString() : undefined)
            ))
        )
    );

    const getRange = (key: string, start: number, end: number): Promise<string> => (
        new Promise((resolve, reject) =>
            getInstance().getrange(key, start, end, (error, data) => (
                error ? reject(new GenericRedisError()) : resolve(data)
            ))
        )
    );

    const deleteKey = (key: string): Promise<void> => (
        new Promise((resolve, reject) =>
            getInstance().del(key, (error) => (
                error ? reject(new GenericRedisError()) : resolve()
            ))
        )
    );

    const getInstance = () => {
        if (!redisClient) {
            throw new RedisClientNotInitializedError();
        }
        return redisClient;
    };

    return {
        setupRedisClient,
        hset,
        hget,
        hincrby,
        bitfield,
        sadd,
        scard,
        set,
        get,
        getRange,
        deleteKey,
    };
})();
