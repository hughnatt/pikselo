import { HOST } from '../../util/constants';
import { Logger } from '../../util/Logger';
import { SocketServer } from '../SocketServer';

export const SocketRepository = (() => {
    const refreshQRAuthURL = (token: string) => (
        SocketServer
            .getInstance()
            .of('/qrauth')
            .emit('refresh-qrcode', { url: `${HOST}/api/v1/qrauth/join/${token}`})
    );

    const notifyPixelChange = (posX: number, posY: number, color: number) => (
        SocketServer
            .getInstance()
            .sockets
            .emit('update-pixel', {
                x: posX,
                y: posY,
                c: color,
            })
    );

    const refreshBoard = () => (
        SocketServer
            .getInstance()
            .sockets
            .emit('refresh-board')
    );

    const refreshQRAuthURLOnConnect = (tokenProvider: () => string) => (
        SocketServer.getInstance().of('/qrauth').on('connection', () => {
            Logger.info('New QR Authenticator connected');
            refreshQRAuthURL(tokenProvider());
        })
    );

    return {
        refreshQRAuthURL,
        notifyPixelChange,
        refreshQRAuthURLOnConnect,
        refreshBoard,
    };
})();
