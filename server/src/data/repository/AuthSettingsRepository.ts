import { RedisClient } from '../RedisClient';

export const AuthSettingsRepository = (() => {

    const getPublicAuthEnabled = (): Promise<boolean> => (
        RedisClient.get('settings:auth:public')
            .then((value) => value === 'true')
    );

    const setPublicAuthEnabled = (enabled: boolean) => (
        RedisClient.set('settings:auth:public', `${enabled}`)
    );

    const getQRAuthEnabled = (): Promise<boolean> => (
        RedisClient.get('settings:auth:qr')
            .then((value) => value === 'true')
    );

    const setQRAuthEnabled = (enabled: boolean) => (
        RedisClient.set('settings:auth:qr', `${enabled}`)
    );

    return {
        getPublicAuthEnabled,
        setPublicAuthEnabled,
        getQRAuthEnabled,
        setQRAuthEnabled,
    };
})();
