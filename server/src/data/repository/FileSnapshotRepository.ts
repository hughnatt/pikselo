import fs from 'fs';

export const FileSnapshotRepository = (() => {
    const saveClick = (
        userId: string,
        posX: number,
        posY: number,
        color: number,
    ) => {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        fs.appendFile('./snapshot.csv', `${Date.now()};${userId};${posX};${posY};${color}\n`, () => {});
    };

    return {
        saveClick,
    };
})();