import { BOARD_HEIGHT, BOARD_WIDTH, COOLDOWN, MAX_HEIGHT, MAX_WIDTH } from '../../util/constants';
import { RedisClient } from '../RedisClient';

export const CanvasSettingsRepository = (() => {
    const getCooldown = (): Promise<number> => (
        RedisClient.get('settings:canvas:cooldown')
            .then((cooldown) => cooldown ? parseInt(cooldown.toString()) : COOLDOWN)
    );

    const setCooldown = (value: number): Promise<void> => (
        RedisClient.set('settings:canvas:cooldown', `${value}`)
    );

    const getCanvasHeight = (): Promise<number> => (
        RedisClient.get('settings:canvas:height')
            .then((height) => height ? parseInt(height.toString()) : BOARD_HEIGHT)
    );

    const setCanvasHeight = (value: number) => (
        RedisClient.set('settings:canvas:height', `${value}`)

    );

    const getCanvasWidth = (): Promise<number> => (
        RedisClient.get('settings:canvas:width')
            .then((width) => width ? parseInt(width.toString()) : BOARD_WIDTH)
    );

    const setCanvasWidth = (value: number) => (
        RedisClient.set('settings:canvas:width', `${value}`)
    );

    const getMaxHeight = (): Promise<number> => (
        RedisClient.get('settings:canvas:max:height')
            .then((height) => height ? parseInt(height.toString()) : MAX_HEIGHT)
    );

    const setMaxHeight = (value: number) => (
        RedisClient.set('settings:canvas:max:height', `${value}`)
    );

    const getMaxWidth = (): Promise<number> => (
        RedisClient.get('settings:canvas:max:width')
            .then((width) => width ? parseInt(width.toString()) : MAX_WIDTH)
    );

    const setMaxWidth = (value: number) => (
        RedisClient.set('settings:canvas:max:width', `${value}`)
    );

    const getReadOnlyStatus = (): Promise<boolean> => (
        RedisClient.get('settings:canvas:readonly')
            .then((readOnly) => readOnly?.toString() === 'true')
    );

    const setReadOnlyStatus = (readOnly: boolean) => (
        RedisClient.set('settings:canvas:readonly', readOnly ? 'true' : 'false')
    );

    return {
        getCanvasHeight,
        setCanvasHeight,
        getCanvasWidth,
        setCanvasWidth,
        getMaxHeight,
        setMaxHeight,
        getMaxWidth,
        setMaxWidth,
        getCooldown,
        setCooldown,
        getReadOnlyStatus,
        setReadOnlyStatus,
    };
})();
