import { PixelModel } from '../../models/services/Pixel';
import { PixelBoard } from '../../models/services/PixelBoard';
import { RedisClient } from '../RedisClient';
import { CanvasSettingsRepository } from './CanvasSettingsRepository';

export const BoardRepository = (() => {
    const getBoard = async (): Promise<PixelBoard> => {
        const boardWidth = await CanvasSettingsRepository.getCanvasWidth();
        const boardHeight = await CanvasSettingsRepository.getCanvasHeight();
        const maxHeight = await CanvasSettingsRepository.getMaxHeight();
        const maxWidth = await CanvasSettingsRepository.getMaxWidth();
        const readOnly = await CanvasSettingsRepository.getReadOnlyStatus();

        return RedisClient.getRange('board', 0, -1)
            .then((data) => {
                const board = {
                    width: boardWidth,
                    height: boardHeight,
                    maxWidth,
                    maxHeight,
                    data: Buffer.from(data, 'utf-8').toString('base64'),
                    readOnly,
                };

                return board;
            });
    };

    const getPixel = async (posX: number, posY: number): Promise<PixelModel> => {
        const maxWidth = await CanvasSettingsRepository.getMaxWidth();

        const offset = (posX + posY * maxWidth) * 4;

        const lastDrawTimePromise = RedisClient.hget(`pixel:${posX}:${posY}`, 'lastDrawTime').then((data) => Number(data?.toString()));
        const userIdPromise = RedisClient.hget(`pixel:${posX}:${posY}`, 'userId');
        const colorPromise = RedisClient.bitfield('board', ['GET', 'u4', offset]).then((data) => data[0]);

        return Promise.all([colorPromise, userIdPromise, lastDrawTimePromise])
            .then(([color, userId, lastDrawTime]) => ({
                x: posX,
                y: posY,
                c: color,
                lastDrawTime,
                userId,
            }));
    };

    const setPixel = async (userId: string, posX: number, posY: number, color: number): Promise<void> => {
        RedisClient.hset(`pixel:${posX}:${posY}`, 'lastDrawTime', `${Date.now()}`);
        RedisClient.hset(`pixel:${posX}:${posY}`, 'userId', userId);

        const maxWidth = await CanvasSettingsRepository.getMaxWidth();

        const offset = (posX + (posY * maxWidth)) * 4;

        return RedisClient.bitfield('board', ['SET', 'u4', offset, color])
            .then(() => { return; });
    };

    const cleanBoard = (): Promise<void> => {
        return RedisClient.deleteKey('board');
    };

    return {
        getBoard,
        getPixel,
        setPixel,
        cleanBoard,
    };
})();
