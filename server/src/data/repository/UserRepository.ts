import { RedisClient } from '../RedisClient';
import { randomBytes, pbkdf2Sync } from 'crypto';
import { InvalidPasswordError } from '../../models/exceptions/InvalidPasswordError';
import { Logger } from '../../util/Logger';
import { CanvasSettingsRepository } from './CanvasSettingsRepository';
import { Role } from '../../models/security/Role';

export const UserRepository = (() => {
    const createUser = (userId: string, role: Role): Promise<void> => {
        const addUserToIndexPromise = RedisClient.sadd('index:user', userId);
        const setRolePromise = RedisClient.hset(`user:${userId}`, 'role', role);
        const setLastDrawTimePromise = RedisClient.hset(`user:${userId}`, 'lastDrawTime', (new Date(0)).toISOString());

        return Promise.all([addUserToIndexPromise, setRolePromise, setLastDrawTimePromise])
            .then(() => { return; });
    };

    const canUserDraw = (userId: string): Promise<boolean> => (
        RedisClient.hget(`user:${userId}`, 'lastDrawTime')
            .then(async (data) => {
                const cooldown = await CanvasSettingsRepository.getCooldown();
                const lastDrawTime = new Date(data ?? 0);
                const lastDrawTimePlusCooldown = new Date(lastDrawTime.getTime() + cooldown * 1000);

                return (lastDrawTimePlusCooldown < new Date());
            })
    );

    const setLastDrawTimeToNow = async (userId: string, color: number): Promise<number> => {
        const currentDate = new Date();
        const cooldown = await CanvasSettingsRepository.getCooldown();
        RedisClient.hincrby(`user:${userId}`, 'stat:pixel:count', 1);
        RedisClient.hincrby(`user:${userId}`, `stat:pixel:color:${color}`, 1);
        return RedisClient.hset(`user:${userId}`, 'lastDrawTime', currentDate.toISOString())
            .then(() => cooldown);
    };

    const getCooldown = (userId: string): Promise<number> => (
        RedisClient.hget(`user:${userId}`, 'lastDrawTime')
            .then(async (data) => {
                const cooldown = await CanvasSettingsRepository.getCooldown();
                const lastDrawTime = new Date(data?.toString() ?? 0);
                const lastDrawTimePlusCooldown = new Date(lastDrawTime.getTime() + cooldown * 1000);

                return (lastDrawTimePlusCooldown.getTime() - Date.now()) / 1000;
            })
    );

    const checkPassword = (login: string, password: string): Promise<boolean> => {
        const hashPromise: Promise<string | undefined> = RedisClient.hget(`user:${login}`, 'hash');
        const saltPromise: Promise<string | undefined> = RedisClient.hget(`user:${login}`, 'salt');

        return Promise.all([hashPromise, saltPromise])
            .then(([expectedHash, salt]) => {
                if (!salt || !expectedHash) {
                    Logger.info('[SECURITY] A user tried to login with password but it is empty. Reporting incident.');
                    return Promise.reject(new InvalidPasswordError());
                }
                const currentHash = pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');
                return currentHash === expectedHash;
            });
    };

    const changePassword = (login: string, password: string): Promise<void> => {
        const salt = randomBytes(16).toString('hex');
        const hash = pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');

        const setSaltPromise = RedisClient.hset(`user:${login}`, 'salt', salt);
        const setHashPromise =  RedisClient.hset(`user:${login}`, 'hash', hash);

        return Promise.all([setSaltPromise, setHashPromise])
            .then(() => { return; });
    };

    const setEulaAccepted = (userId: string) => {
        return RedisClient.hset(`user:${userId}`, 'eulaStatus', 'accepted')
            .then(() => { return; });
    };

    const getEulaStatus = (userId: string): Promise<boolean> => {
        return RedisClient.hget(`user:${userId}`, 'eulaStatus')
            .then((status) => {
                return status === 'accepted';
            });
    };

    const getUserCount = (): Promise<number> => (
        RedisClient.scard('index:user')
    );

    const registerEmail = (userId: string, email: string): Promise<number> => (
        RedisClient.hset(`user:${userId}`, 'email', email)
    );

    const banUser = (userId: string): Promise<number> => (
        RedisClient.hset(`user:${userId}`, 'ban', 'true')
    );
    const isUserBan = (userId: string): Promise<boolean> => (
        RedisClient.hget(`user:${userId}`, 'ban').then((data) => data?.toString() === 'true')
    );

    const getRole = (userId: string): Promise<Role> => (
        RedisClient.hget(`user:${userId}`, 'role').then((data) => data?.toString() as Role)
    );

    return {
        setLastDrawTimeToNow,
        getCooldown,
        canUserDraw,
        createUser,
        checkPassword,
        changePassword,
        getUserCount,
        setEulaAccepted,
        getEulaStatus,
        registerEmail,
        banUser,
        isUserBan,
        getRole,
    };
})();
