import express, { NextFunction, Request, Response } from 'express';
import { authorize } from '../helpers/security';
import { Role } from '../models/security/Role';
import { AdminService } from '../services/AdminService';

const router = express.Router();

// Authenticate admin user
router.post('/auth/login', (req: Request, res: Response, next: NextFunction) => {
    const login = req.body.login;
    const password = req.body.password;

    AdminService.authLogin(login, password)
        .then((session) => res.status(200).send(session))
        .catch((error) => next(error));
});

// Register a new admin user
router.post('/auth/register', authorize(Role.ADMIN), (req: Request, res: Response, next: NextFunction) => {
    const login = req.body.login;
    const password = req.body.password;

    AdminService.authRegister(login, password)
        .then(() => res.status(200).send())
        .catch((error) => next(error));
});

// Refresh access token
router.post('/auth/refresh', (req: Request, res: Response, next: NextFunction) => {
    const refreshToken = req.body.refresh_token;
    AdminService.authRefresh(refreshToken)
        .then((accessToken) => res.status(200).send({ accessToken }))
        .catch((error) => next(error));
});

// Get dashboard with multiple informations
router.get('/dashboard', authorize(Role.ADMIN), (_req: Request, res: Response, next: NextFunction) => {
    AdminService.getDashboard()
        .then((dashboard) => res.status(200).send({ dashboard }))
        .catch((error) => next(error));
});

// Get canvas settings
router.get('/settings/canvas', authorize(Role.ADMIN), (_req: Request, res: Response, next: NextFunction) => {
    AdminService.getCanvasSettings()
        .then((canvasSettings) => res.status(200).send({ canvasSettings }))
        .catch((error) => next(error));
});

// Update canvas settings
router.put('/settings/canvas', authorize(Role.ADMIN), (req: Request, res: Response, next: NextFunction) => {
    AdminService.updateCanvasSettings(req.body)
        .then((canvasSettings) => res.status(200).send({ canvasSettings }))
        .catch((error) => next(error));
});

router.get('/settings/auth', authorize(Role.ADMIN), (req: Request, res: Response, next: NextFunction) => {
    AdminService.getAuthSettings()
        .then((authSettings) => res.status(200).send({ authSettings }))
        .catch((error) => next(error));
});

router.put('/settings/auth', authorize(Role.ADMIN), (req: Request, res: Response, next: NextFunction) => {
    AdminService.updateAuthSettings(req.body)
        .then((authSettings) => res.status(200).send({ authSettings }))
        .catch((error) => next(error));
});

export { router as adminRouter };
