import express, { NextFunction, Request, Response } from 'express';

import { QRAuthService } from '../services/QRAuthService';
import { REDIRECT_URL } from '../util/constants';

const router = express.Router();

// Manual URL refresh
router.get('/refresh', (_req: Request, res: Response, next: NextFunction) => {
    QRAuthService.refreshCode()
        .then(() => {
            res.status(200).send();
        })
        .catch((error) => {
            next(error);
        });
});

// Public route for joining, can be disabled in config, will return a redirect
router.get('/join/public/redirect', (req: Request, res: Response, next: NextFunction) => {
    QRAuthService.joinPublicUser()
        .then((jwt) => {
            res.redirect(302, REDIRECT_URL + `/${jwt}`);
        })
        .catch((error) => {
            next(error);
        });
});

// Public route for joining, can be disabled in config, will return game token
router.get('/join/public', (req: Request, res: Response, next: NextFunction) => {
    QRAuthService.joinPublicUser()
        .then((jwt) => {
            res.status(200).send({ token: jwt });
        })
        .catch((error) => {
            next(error);
        });
});

// Exchange QR-Auth token with game token
router.get('/join/:token', (req: Request, res: Response, next: NextFunction) => {
    QRAuthService.joinUser(req.params.token)
        .then((jwt) => {
            // Putting the jwt directly in the URL is up to debate.
            // It seems enough for the level of security we are looking for,
            // which is quite low overall.
            res.redirect(302, REDIRECT_URL + `/${jwt}`);
        })
        .catch((error) => {
            next(error);
        });
});

export { router as qrAuthRouter };
