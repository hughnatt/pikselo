import express, { Request, Response, NextFunction } from 'express';
import { authorize } from '../helpers/security';
import { Role } from '../models/security/Role';
import { UserService } from '../services/UserService';

const router = express.Router();

router.get('/status', authorize(Role.USER), (_req: Request, res: Response, next: NextFunction) => {
    UserService.getStatus(res.locals.userId)
        .then((status) => {
            res.status(200).send(status);
        })
        .catch((error) => {
            next(error);
        });
});

router.put('/eula', authorize(Role.USER), (req: Request, res: Response, next: NextFunction) => {
    UserService.acceptEula(res.locals.userId, req.body.email)
        .then(() => {
            res.status(200).send();
        })
        .catch((error) => {
            next(error);
        });
});

router.post('/ban/:id', authorize(Role.ADMIN), (req: Request, res: Response, next: NextFunction) => {
    UserService.banUser(req.params.id)
        .then(() => {
            res.status(200).send();
        })
        .catch((error) => {
            next(error);
        });
});

export { router as userRouter };