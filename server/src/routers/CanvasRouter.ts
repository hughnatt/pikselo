import express, { NextFunction, Request, Response } from 'express';

import { authorize } from '../helpers/security';
import { InvalidColorError } from '../models/exceptions/InvalidColorError';
import { Role } from '../models/security/Role';
import { CanvasService } from '../services/CanvasService';

const router = express.Router();

// Get pixel board
router.get('/board', authorize(Role.USER), (_req: Request, res: Response, next: NextFunction) => {
    CanvasService.getBoard()
        .then((board) => {
            res.status(200).send(board);
        })
        .catch((error) => {
            next(error);
        });
});

// Set pixel color
router.put('/board/pixel', authorize(Role.USER), (req: Request, res: Response, next: NextFunction) => {
    const posX = Number(req.query.x);
    const posY = Number(req.query.y);
    const color = Number(req.query.c);
    const userId = res.locals.userId;
    const role = res.locals.userRole;

    if (color > 15 || color < 0) { throw new InvalidColorError(); }

    CanvasService.setPixel(userId, posX, posY, color, role)
        .then((delay) => {
            res.status(200).send({ cooldown: delay });
        })
        .catch((error) => {
            next(error);
        });
});

// Get single pixel info
router.get('/board/pixel', authorize(Role.USER), (req: Request, res: Response, next: NextFunction) => {
    const pixelX = Number(req.query.x);
    const pixelY = Number(req.query.y);

    CanvasService.getPixel(pixelX, pixelY)
        .then((pixel) => {
            res.status(200).send(pixel);
        })
        .catch((error) => {
            next(error);
        });
});

// Clean board
router.delete('/board', authorize(Role.ADMIN), (req: Request, res: Response, next: NextFunction) => {
    CanvasService.cleanBoard()
        .then(() => {
            res.status(200).send();
        })
        .catch((error) => {
            next(error);
        });
});

export { router as canvasRouter };
