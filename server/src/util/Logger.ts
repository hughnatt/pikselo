import winston from 'winston';

import { LoggerNotInitializedError } from '../models/exceptions/LoggerNotInitializedError';

export const Logger = (() => {
    let instance: winston.Logger;

    const logsFormat = winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(({ level, message, timestamp }) => {
            return `[${timestamp}](${level}): ${message}`;
        }),
    );

    const logsLevel = 'debug';

    const setupLogger = (): void => {
        instance = winston.createLogger({
            transports: [new winston.transports.Console()],
            format: logsFormat,
            level: logsLevel,
        });

        debug('New logger instance created');
    };

    const getInstance = () => {
        if (!instance) {
            throw new LoggerNotInitializedError();
        }
        return instance;
    };

    const debug = (message: string): void => {
        getInstance().debug(message);
    };

    const info = (message: string): void => {
        getInstance().info(message);
    };

    const warn = (message: string, error?: Error): void => {
        warnOrError('warn', message, error);
    };

    const error = (message: string, error?: Error): void => {
        warnOrError('error', message, error);
    };

    const warnOrError = (type: 'warn' | 'error', message: string, errorObject?: Error): void => {
        if (errorObject) {
            message += JSON.stringify(errorObject);
        }
        getInstance().log(type, message);
    };

    return {
        setupLogger,
        debug,
        info,
        warn,
        error,
        logsFormat,
        logsLevel,
    };
})();
