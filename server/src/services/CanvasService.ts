import { BoardRepository } from '../data/repository/BoardRepository';
import { CanvasSettingsRepository } from '../data/repository/CanvasSettingsRepository';
import { FileSnapshotRepository } from '../data/repository/FileSnapshotRepository';
import { SocketRepository } from '../data/repository/SocketRepository';
import { UserRepository } from '../data/repository/UserRepository';
import { SetPixelCooldownError } from '../models/exceptions/SetPixelCooldownError';
import { SetPixelReadOnlyError } from '../models/exceptions/SetPixelReadOnlyError';
import { Role } from '../models/security/Role';
import { PixelModel } from '../models/services/Pixel';
import { PixelBoard } from '../models/services/PixelBoard';


export const CanvasService = (() => {
    const setPixel = (userId: string, posX: number, posY: number, color: number, role: Role): Promise<number> => (
        Promise.all([
            UserRepository.getCooldown(userId),
            CanvasSettingsRepository.getReadOnlyStatus(),
        ]).then(([cooldown, readOnly]) => {
            if (readOnly) {
                return Promise.reject(new SetPixelReadOnlyError());
            } else if (cooldown > 0 && role !== Role.ADMIN) {
                return Promise.reject(new SetPixelCooldownError(cooldown));
            } else {
                return BoardRepository.setPixel(userId, posX, posY, color)
                    .then(() => {
                        SocketRepository.notifyPixelChange(posX, posY, color);
                        FileSnapshotRepository.saveClick(userId, posX, posY, color);

                        return UserRepository.setLastDrawTimeToNow(userId, color);
                    });
            }
        })
    );

    const getBoard = (): Promise<PixelBoard> => (
        BoardRepository.getBoard()
    );

    const getPixel = (posX: number, posY: number): Promise<PixelModel> => (
        BoardRepository.getPixel(posX, posY)
    );

    const cleanBoard = (): Promise<void> => (
        BoardRepository.cleanBoard()
    );

    return {
        setPixel,
        getBoard,
        getPixel,
        cleanBoard,
    };
})();
