import { AuthSettingsRepository } from '../data/repository/AuthSettingsRepository';
import { CanvasSettingsRepository } from '../data/repository/CanvasSettingsRepository';
import { SocketRepository } from '../data/repository/SocketRepository';
import { UserRepository } from '../data/repository/UserRepository';
import { generateSessionForRole, getAccessTokenFromRefreshToken } from '../helpers/security';
import { InvalidPasswordError } from '../models/exceptions/InvalidPasswordError';
import { Role } from '../models/security/Role';
import { AuthSettings } from '../models/services/AuthSettings';
import { CanvasSettings } from '../models/services/CanvasSettings';
import { Dashboard } from '../models/services/Dashboard';
import { JWT } from '../models/services/JWT';
import { Session } from '../models/services/Session';

export const AdminService = (() => {
    const authLogin = (login: string, password: string): Promise<Session> => (
        UserRepository.checkPassword(login, password)
            .then((isPasswordValid) => {
                if (isPasswordValid) {
                    return Promise.resolve(generateSessionForRole(login, Role.ADMIN));
                } else {
                    return Promise.reject(new InvalidPasswordError());
                }
            })
    );

    const authRegister = (login: string, password: string): Promise<void> => {
        const createUserPromise = UserRepository.createUser(login, Role.ADMIN);
        const changePasswordPromise = UserRepository.changePassword(login, password);

        return Promise.all([createUserPromise, changePasswordPromise])
            .then(() => { return; });
    };

    const authRefresh = (refreshToken: JWT): Promise<JWT> => (
        getAccessTokenFromRefreshToken(refreshToken)
    );

    const getDashboard = (): Promise<Dashboard> => {
        const userCountPromise = UserRepository.getUserCount();

        return Promise.all([userCountPromise])
            .then(([userCount]) => ({
                userCount,
            }));
    };

    const getCanvasSettings = (): Promise<CanvasSettings> => {
        const heightPromise = CanvasSettingsRepository.getCanvasHeight();
        const widthPromise = CanvasSettingsRepository.getCanvasWidth();
        const cooldownPromise = CanvasSettingsRepository.getCooldown();
        const readOnlyPromise = CanvasSettingsRepository.getReadOnlyStatus();

        return Promise.all([
            heightPromise,
            widthPromise,
            cooldownPromise,
            readOnlyPromise,
        ]).then(([boardHeight, boardWidth, cooldown, readOnly]) => ({
            boardHeight,
            boardWidth,
            cooldown,
            readOnly,
        }));
    };

    const updateCanvasSettings = (canvasSettings: Partial<CanvasSettings>): Promise<CanvasSettings> => {
        if (canvasSettings.boardHeight !== undefined) {
            CanvasSettingsRepository.setCanvasHeight(canvasSettings.boardHeight);
        }

        if (canvasSettings.boardWidth !== undefined) {
            CanvasSettingsRepository.setCanvasWidth(canvasSettings.boardWidth);
        }

        if (canvasSettings.cooldown !== undefined) {
            CanvasSettingsRepository.setCooldown(canvasSettings.cooldown);
        }

        if (canvasSettings.readOnly !== undefined) {
            CanvasSettingsRepository.setReadOnlyStatus(canvasSettings.readOnly);
        }

        SocketRepository.refreshBoard();

        return getCanvasSettings();
    };

    const getAuthSettings = (): Promise<AuthSettings> => {
        const publicAuthEnabledPromise = AuthSettingsRepository.getPublicAuthEnabled();
        const qrAuthEnabledPromise = AuthSettingsRepository.getQRAuthEnabled();

        return Promise.all([
            publicAuthEnabledPromise,
            qrAuthEnabledPromise,
        ]).then(([publicAuthEnabled, qrAuthEnabled]) => ({
            publicAuthEnabled,
            qrAuthEnabled,
        }));
    };

    const updateAuthSettings = (authSettings: Partial<AuthSettings>): Promise<AuthSettings> => {
        if (authSettings.publicAuthEnabled !== undefined) {
            AuthSettingsRepository.setPublicAuthEnabled(authSettings.publicAuthEnabled);
        }

        if (authSettings.qrAuthEnabled !== undefined) {
            AuthSettingsRepository.setQRAuthEnabled(authSettings.qrAuthEnabled);
        }

        return getAuthSettings();
    };

    return {
        authLogin,
        authRegister,
        authRefresh,
        getDashboard,
        getCanvasSettings,
        updateCanvasSettings,
        getAuthSettings,
        updateAuthSettings,
    };
})();
