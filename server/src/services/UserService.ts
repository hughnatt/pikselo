import { UserRepository } from '../data/repository/UserRepository';
import { CantRevokeAdminError } from '../models/exceptions/CantRevokeAdminError';
import { Role } from '../models/security/Role';
import { UserStatus } from '../models/services/UserStatus';

export const UserService = (() => {
    const getStatus = (userId: string): Promise<UserStatus> => (
        Promise.all([
            UserRepository.getEulaStatus(userId),
            UserRepository.getCooldown(userId),
        ]).then(([eulaAccepted, cooldown]) => ({
            eulaAccepted,
            cooldown,
        }))
    );

    const acceptEula = (userId: string, email?: string) => {
        if (email) {
            UserRepository.registerEmail(userId, email);
        }

        return UserRepository.setEulaAccepted(userId);
    };

    const banUser = (userId: string) => {
        return UserRepository.getRole(userId).then((role) => {
            if (role === Role.ADMIN) {
                throw new CantRevokeAdminError();
            } else {
                return UserRepository.banUser(userId);
            }
        });
    };

    return {
        getStatus,
        acceptEula,
        banUser,
    };
})();