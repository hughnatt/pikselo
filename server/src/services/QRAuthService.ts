import { sign as createJWT } from 'jsonwebtoken';
import { v4 as generateUUID } from 'uuid';
import { AuthSettingsRepository } from '../data/repository/AuthSettingsRepository';
import { SocketRepository } from '../data/repository/SocketRepository';
import { UserRepository } from '../data/repository/UserRepository';

import { InvalidQRCodeTokenError } from '../models/exceptions/InvalidQRCodeTokenError';
import { PublicAuthDisabledError } from '../models/exceptions/PublicAuthDisabledError';
import { QRAuthDisabledError } from '../models/exceptions/QRAuthDisabledError';
import { Role } from '../models/security/Role';
import { JWT } from '../models/services/JWT';
import { JWT_SECRET } from '../util/constants';
import { Logger } from '../util/Logger';


export const QRAuthService = (() => {
    let currentToken = generateUUID();

    const refreshCode = (): Promise<void> => (
        new Promise((resolve) => {
            currentToken = generateUUID();
            SocketRepository.refreshQRAuthURL(currentToken);

            resolve();
        })
    );

    const joinUser = async (token: string): Promise<JWT> => {
        const isQRAuthEnabled = await AuthSettingsRepository.getQRAuthEnabled();

        if (!isQRAuthEnabled) {
            return Promise.reject(new QRAuthDisabledError());
        }

        if (token === currentToken) {
            currentToken = generateUUID();
            SocketRepository.refreshQRAuthURL(currentToken);

            Logger.info('Registering new qrauth user');

            // JWT Generation
            const jwt = createJWT({
                id: token,
                role: Role.USER,
            }, JWT_SECRET, { expiresIn: '72h' });

            // User creation
            return UserRepository.createUser(token, Role.USER)
                .then(() => jwt);
        } else {
            return Promise.reject(new InvalidQRCodeTokenError());
        }
    };

    const joinPublicUser = async (): Promise<JWT> => {
        const isPublicAuthEnabled = await AuthSettingsRepository.getPublicAuthEnabled();

        if (!isPublicAuthEnabled) {
            return Promise.reject(new PublicAuthDisabledError());
        }

        const userToken = generateUUID();

        Logger.info('Registering new public user');

        // JWT Generation
        const jwt = createJWT({
            id: userToken,
            role: Role.USER,
        }, JWT_SECRET, { expiresIn: '72h' });

        // User creation
        return UserRepository.createUser(userToken, Role.USER)
            .then(() => jwt as JWT);
    };

    const setupQRAuth = () => {
        SocketRepository.refreshQRAuthURLOnConnect(() => currentToken);
    };

    return {
        refreshCode,
        joinUser,
        joinPublicUser,
        setupQRAuth,
    };
})();
