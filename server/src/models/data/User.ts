import { Role } from '../security/Role';

export type User = {
    id: string,
    role: Role,
    lastDrawTime: Date,
    lastPixelX: number,
    lastPixelY: number,
}
