export type CanvasSettings = {
    boardWidth: number,
    boardHeight: number,
    cooldown: number,
    readOnly: boolean,
};
