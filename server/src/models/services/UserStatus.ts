export type UserStatus = {
    eulaAccepted: boolean,
    cooldown: number,
}