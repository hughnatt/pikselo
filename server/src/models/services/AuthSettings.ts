export type AuthSettings = {
    publicAuthEnabled: boolean,
    qrAuthEnabled: boolean,
};
