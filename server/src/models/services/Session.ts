import { JWT } from './JWT';

export type Session = {
    refreshToken: JWT,
    accessToken: JWT,
}
