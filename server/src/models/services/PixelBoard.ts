export type PixelBoard = {
    width: number,
    height: number,
    data: string,
    readOnly: boolean,
    maxWidth: number,
    maxHeight: number,
};
