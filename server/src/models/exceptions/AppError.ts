export class AppError extends Error {
    status: number;
    data: Record<string, unknown> | undefined;

    constructor(status: number, message: string, data?: Record<string, unknown>) {
        super(message);
        this.status = status;
        this.data = data;
    }
}
