import { AppError } from './AppError';

export class GenericRedisError extends AppError {
    constructor() {
        super(500, 'Internal server error, retry later or contact an administrator');
    }
}
