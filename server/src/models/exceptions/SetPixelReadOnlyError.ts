import { AppError } from './AppError';

export class SetPixelReadOnlyError extends AppError {
    constructor() {
        super(400, 'Board is readonly', { cooldown: 0 });
    }
}
