import { AppError } from './AppError';

export class CantRevokeAdminError extends AppError {
    constructor() {
        super(400, 'You cant revoke an administrative account');
    }
}
