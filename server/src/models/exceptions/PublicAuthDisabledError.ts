import { AppError } from './AppError';

export class PublicAuthDisabledError extends AppError {
    constructor() {
        super(404, 'Public auth is disabled');
    }
}
