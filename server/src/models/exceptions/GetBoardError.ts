import { AppError } from './AppError';

export class GetBoardError extends AppError {
    constructor() {
        super(500, 'Failed to get board information');
    }
}
