import { AppError } from './AppError';

export class UserRevokedPermissionError extends AppError {
    constructor() {
        super(401, 'This user id has been revoked');
    }
}
