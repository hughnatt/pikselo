import { AppError } from './AppError';

export class MissingAuthorizationTokenError extends AppError {
    constructor() {
        super(401, 'Missing authorization token');
    }
}
