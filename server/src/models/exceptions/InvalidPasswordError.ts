import { AppError } from './AppError';

export class InvalidPasswordError extends AppError {
    constructor() {
        super(401, 'Invalid password');
    }
}
