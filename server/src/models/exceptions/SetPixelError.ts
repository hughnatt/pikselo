import { AppError } from './AppError';

export class SetPixelError extends AppError {
    constructor() {
        super(500, 'Failed to set pixel');
    }
}
