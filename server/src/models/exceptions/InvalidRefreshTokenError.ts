import { AppError } from './AppError';

export class InvalidRefreshTokenError extends AppError {
    constructor() {
        super(401, 'Invalid refresh token');
    }
}
