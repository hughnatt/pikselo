import { AppError } from './AppError';

export class SetPixelCooldownError extends AppError {
    constructor(cooldown: number) {
        super(400, 'Cooldown period', { cooldown });
    }
}
