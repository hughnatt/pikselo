import { AppError } from './AppError';

export class GetPixelError extends AppError {
    constructor() {
        super(500, 'Failed to get pixel information');
    }
}
