import { AppError } from './AppError';

export class QRAuthDisabledError extends AppError {
    constructor() {
        super(404, 'Authentication with QRCode is disabled');
    }
}
