import { AppError } from './AppError';

export class InvalidQRCodeTokenError extends AppError {
    constructor() {
        super(401, 'Invalid token');
    }
}
