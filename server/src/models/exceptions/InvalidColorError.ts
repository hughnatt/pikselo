import { AppError } from './AppError';

export class InvalidColorError extends AppError {
    constructor() {
        super(400, 'Invalid color');
    }
}
