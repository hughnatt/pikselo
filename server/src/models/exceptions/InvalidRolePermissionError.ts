import { AppError } from './AppError';

export class InvalidRolePermissionError extends AppError {
    constructor() {
        super(401, 'Invalid role');
    }
}
