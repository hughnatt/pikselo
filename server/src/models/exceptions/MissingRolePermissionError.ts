import { AppError } from './AppError';

export class MissingRolePermissionError extends AppError {
    constructor() {
        super(401, 'Role property is missing from authorization token');
    }
}
